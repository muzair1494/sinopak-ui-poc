import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Modal,Button} from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    // paddingTop: '150px',
    padding: '5px',
    // marginTop:'10px',
    width:'50%'
  },
}));

export default function TransitionsModal(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      {/* <button type="button" onClick={handleOpen}>
        Add
      // </button> */}
       {/* <Modal */}
      {/* //   aria-labelledby="transition-modal-title"
      //   aria-describedby="transition-modal-description"
      //   className={classes.modal}
      //   open={props.openForm}
      //   onClose={handleClose}
      //   closeAfterTransition
      //   BackdropComponent={Backdrop}
      //   BackdropProps={{ */}
      {/* //     timeout: 500,
      //   }}
      //   style={{height:'400px',overflowY:'scroll'}}
      // >
      //   <Fade in={props.openForm}> */}

        <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    open={props.openForm}
                    // onClose={this.handleModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={props.openForm}>
          {/* <h2 id="transition-modal-title">{props.title}</h2> */}
            {props.children}
        </Fade>
      </Modal>
    </div>
  );
}
