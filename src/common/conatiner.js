import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent:'center',
    '& > *': {
      margin: theme.spacing(1),
      padding: '10px',
      width: '100%',
      height: 'auto',
    },
  },
}));

export default function Container(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper elevation={3} >
        {props.children}
      </Paper>
    </div>
  );
}
