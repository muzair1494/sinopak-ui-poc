import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export const TwoItems =(props)=>{
return <ListItem   key={props.key}>
    <ListItemText  primary={props.label} />
    <ListItemText  secondary={props.content} />
</ListItem>
}