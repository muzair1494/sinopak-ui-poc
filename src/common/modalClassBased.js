import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


export default class Product extends Component {
  state = {
    open: false
  }
  handleClose = () => {
    this.setState({ open: false })
  };
  componentWillReceiveProps(nextporps) {
    this.setState({ open: nextporps.open })
  }
  render() {
    const { vendor, tax, shipment, delivery, product_name, shape, color, image, barcode, range, cost, status } = this.props.detail
    const { open } = this.state
    return (<div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        open={open}
        onClose={this.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div style={{
            backgroundColor: "white",
            border: '2px solid #000',
            padding: '10px',
            width: '30%', height: '500px', overflow: 'scroll'
          }}>
            <h3>Detail</h3>


            <Table aria-label="simple table">
              <TableBody>
                <TableRow>
                  <TableCell>Image:</TableCell>
                  <TableCell>{image}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Product Name:</TableCell>
                  <TableCell>{product_name}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Shape:</TableCell>
                  <TableCell>{shape}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Color:</TableCell>
                  <TableCell>{color}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>barcode</TableCell>
                  <TableCell>{barcode}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>vendor:</TableCell>
                  <TableCell>{vendor}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>range:</TableCell>
                  <TableCell>{range}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>status:</TableCell>
                  <TableCell>{status}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>cost:</TableCell>
                  <TableCell>{cost}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>shipment cost:</TableCell>
                  <TableCell>{shipment}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>delivery cost:</TableCell>
                  <TableCell>{delivery}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>tax rate:</TableCell>
                  <TableCell>{tax}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>total:</TableCell>
                  <TableCell>23,456 /-</TableCell>
                </TableRow>
              </TableBody>
            </Table>

          </div>
        </Fade>
      </Modal>
    </div>)
  }
}