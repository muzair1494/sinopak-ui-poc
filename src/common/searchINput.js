import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export const GetSearchData = ({name,search,getSearched,title,onBlur,val}) => {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField id="standard-basic" name={name} label={title} value={search} onChange={getSearched} onBlur={(e)=>onBlur(e,val)} />
    </form>
  );
}