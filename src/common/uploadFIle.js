import React from 'react';
import Button from '@material-ui/core/Button';

export default function FileUpload(props){
    return <Button
    variant="contained"
    component="label"
    style={{float:`${props.fl}`}}
  >
  Upload File
    <input
      type="file"
      onChange={(e)=>props.change(e)}
      style={{ display: "none" }}
    />
  </Button>
} 