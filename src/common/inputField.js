import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '50ch'
    },
  },
}));

export const  InputField=({change,name,lable})=> {
  const classes = useStyles();
  return (
    <form  className={classes.root} noValidate autoComplete="off">
      <TextField onChange={change} id="standard-basic" name={name} label={lable} />
    </form>
  );
}