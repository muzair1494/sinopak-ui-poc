import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      height: 140,
      width: 100,
    },
    control: {
      padding: theme.spacing(2),
    },
  }));


export const GridContainer =(props)=>{
  const classes = useStyles();
  return <Grid container className={classes.root} spacing={2}>
            <Grid item xs={props.columns}>
                {props.children}
            </Grid>
        </Grid>
  }


//   under development