import React from 'react';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

export default function DropDown(props){
    return <FormControl style={{minWidth:'185px',width:'100%',float:`${props.floating}`}}>
        <InputLabel id="demo-simple-select-label">{props.title}</InputLabel>
        <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={props.value}
            name={props.name}
            onChange={props.change}
        >
            {props.children}
        </Select>
      </FormControl>
} 