import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: 0,
      width: '25ch',
    },
  },
}));

export default function BorderedInputField({change,name,lable}) {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField id="outlined-basic" onChange={change} name={name} label={lable}  variant="outlined" />
    </form>
  );
}
