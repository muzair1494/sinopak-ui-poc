import React from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './productList.css';
import { Title } from './../list/propslist';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { IconButton, withStyles } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { getPOS} from './../../actions/simpleAction'
import Button from '@material-ui/core/Button';
import TransitionsModal from './../../common/modal'
import Grid from '@material-ui/core/Grid';

import DropDown from './../../common/dropDownSelect'
import MenuItem from '@material-ui/core/MenuItem';


const ExpansionPanelSummaryStyle = withStyles({
    root: {
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        minHeight: '50px !important',
        '&$expanded': {
            minHeight: '50px !important',
        },
    }
})(MuiExpansionPanelSummary);
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: '#eee'
    }
}))(TableCell);
const useStyles = makeStyles((theme) => ({

    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));


function ProductAccordian(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <ExpansionPanel expanded={expanded === props.item.product_uuid} onChange={handleChange(props.item.product_uuid)}>
            <ExpansionPanelSummaryStyle
                expandIcon={<ExpandMoreIcon />}
                aria-controls={"panel-content" + props.keyIndex}
                id={"panel-header" + props.keyIndex}
            >
                <Typography className={classes.heading}>{props.item.category}</Typography>
                <Typography className={classes.secondaryHeading}>{props.item.description}</Typography>
            </ExpansionPanelSummaryStyle>
            <ExpansionPanelDetails>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table" size="small">
                        <TableHead>
                            <TableRow>
                                
                                <StyledTableCell>Name</StyledTableCell>
                                {props.keyss !== 'sales' && <StyledTableCell>Image</StyledTableCell>}
                                <StyledTableCell>Barcode</StyledTableCell>
                                {props.keyss !== 'sales' && <StyledTableCell>Weight</StyledTableCell>}
                                {props.keyss == 'sales' && <StyledTableCell>Add Cost</StyledTableCell> }
                                {props.keyss == 'sales' && <StyledTableCell>Select</StyledTableCell> }
                                {props.keyss !== 'sales' && <StyledTableCell align="center">Action</StyledTableCell>}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.item.items.map((item, index) => {
                                return (
                                    <TableRow  key={item.uuid}>
                                        <TableCell>{item.name}</TableCell>
                                        {props.keyss !== 'sales' && <TableCell>{item.image}</TableCell>}
                                        <TableCell>{item.barcode}</TableCell>
                                        {props.keyss !== 'sales' && <TableCell>{item.weight}</TableCell>}
                                       {props.keyss == 'sales' && <TableCell><Button onClick={()=>props.passData(props.item.product_uuid,item.name)} variant="contained">Add Cost</Button></TableCell>}
                                       {props.keyss == 'sales' && <TableCell><Button onClick={()=>props.getOnedata(item,props.item.category,props.item.description)} variant="contained">Select</Button></TableCell>}
                                        {props.keyss !== 'sales' && <TableCell align="center">
                                            <IconButton aria-label="edit" size="small" onClick={() => alert('edit')}>
                                                <EditIcon />
                                            </IconButton>
                                            <IconButton aria-label="delete" color="secondary" size="small" onClick={() => alert('delete?')}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>}
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}

class ProductList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            loading: true,
            expadedState:'',
            openForm:false,
            openCatMod:false,
            category:'',
            description:'',
            product_name:'',
            product_barcode:'',
            product_weight:'',
            product_image:'',
            product_cost:'',
            cats: ["DINNER SET","TEA SET"],
            cat_name:''
        };
    }

    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/productv2').then((res) => {
            this.setState({ products: res.data.products, loading: false })
        })
    }
    getOnedata =(item,category,description)=>{
        let { uuid, name, image, barcode, weight, cost} = item
        var values ={
            uuid, name, image, barcode, weight, cost, category,description
        }
        this.props.getPOS(values)
    }
    addcost=(itemid,prodname)=>{
        console.log(itemid,prodname)
    }
    handleModal=()=>{
        this.setState({openForm:!this.state.openForm,product_name:'',product_image:'',
    product_cost:'',product_image:'',product_weight:'',description:'',category:''})
    }
    onSubmit=()=>{
       console.log('get it')
        let arr = []
        let obj ={
            uuid: '56gd',
            name:this.state.product_name,
            image: this.state.product_image,
            barcode: this.state.product_barcode,
            weight: this.state.product_weight,
            cost: this.state.product_cost
        }
        arr.push(obj)
        var values ={
            product_uuid: 'abcd12',
            category: this.state.category,
            description: this.state.description,
            items: arr
        }
        let filterData = this.state.products.find(item=>item.category == this.state.category && item.description == this.state.description)
        if(filterData != undefined){
          this.setState({products: this.state.products.map(item=>{
            if(item.category == this.state.category && item.description == this.state.description){
                console.log(item.items,'working')
                item.items.push(obj)
            }
            return item
          })
        },()=>console.log(this.state.products,'chekProducts'))
        }else{
          this.setState({products:[...this.state.products,values]})
        }
        this.handleModal()
     
    }
    handleChange2=(e)=>{
        this.setState({[e.target.name]: e.target.value})
    }
    addNewCategory=()=>{
        this.setState({openCatMod:!this.state.openCatMod})
    }
    categoryAdd =()=>{
        this.setState({cats:[...this.state.cats,this.state.cat_name]})
        this.addNewCategory()
    }
    render() {
        const {openForm, category} = this.state
        return (
            <div className="container-fluid">
                <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                    <Title sty listTitle="Products List" />
                    <Button variant="contained" color="primary" onClick={this.handleModal}>
                        Add Product
                    </Button>
                </div>
                <div style={{ width: '100%' }}>
                    {this.state.products.map((item, index) => {
                        return (
                            <ProductAccordian keyss={this.props.keyss} passData={this.addcost} getOnedata={this.getOnedata} item={item} keyIndex={index.toString()} />
                        );
                    })}
                </div>
                {this.state.loading && <div className="loadingTr">Loading ...</div>}
                <TransitionsModal openForm={openForm}>
                <div style={{
                            backgroundColor: "white",
                            width: '400px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                                <h3 style={{ margin: 0 }}>Create Product</h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                {/* <TextField id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Name" fullWidth value={this.state.name} onChange={e => this.setState({ name: e.target.value })} /> */}
                                <div style={{ marginTop: '10px' }}></div>
                                <Grid container spacing={2}>
      <Grid item xs={7}>
      <DropDown 
                                    floating="right"  
                                    title="Category Select"
                                    value={category}
                                    name="category"
                                    change={this.handleChange2}
                                    >
                                        {this.state.cats.map(item=><MenuItem value={item}>{item}</MenuItem>)}
                                  </DropDown>   
                                </Grid>
                                <Grid item xs={5}>
                                <Button variant="contained" color="primary" onClick={this.addNewCategory} style={{ marginTop:'10px',width: '100%' }}>
                                    Add Category
                                </Button>
                                
                                  </Grid>
                                  </Grid>
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-contact" error={this.state.contactError} helperText={this.state.contactError ? 'required' : null} label="Description" fullWidth value={this.state.description} onChange={e => this.setState({ description: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Product name" fullWidth value={this.state.product_name} onChange={e => this.setState({ product_name: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Product Weight" fullWidth value={this.state.product_weight} onChange={e => this.setState({ product_weight: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Product Barcode" fullWidth value={this.state.product_barcode} onChange={e => this.setState({ product_barcode: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Product Image" fullWidth value={this.state.product_image} onChange={e => this.setState({ product_image: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Product Cost" fullWidth value={this.state.product_cost} onChange={e => this.setState({ product_cost: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.onSubmit} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.handleModal}>cancel</Button>
                                </div>
                            </div>
                        </div>
                </TransitionsModal>
                <TransitionsModal openForm={this.state.openCatMod}>
                <div style={{
                            backgroundColor: "white",
                            width: '400px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                                <h3 style={{ margin: 0 }}>Create Category</h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                <TextField id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Category Name" fullWidth value={this.state.cat_name} onChange={e => this.setState({ cat_name: e.target.value })} />
                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.categoryAdd} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.addNewCategory}>cancel</Button>
                                </div>
                            </div>
                        </div>
                </TransitionsModal>      
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    getPOS: (value)=> dispatch(getPOS(value))
   })
const mapStateToProps = state => ({
    grn: state.simpleReducer.GRN_List,
    pending: state.simpleReducer.pending_GRN,
   })


export default connect(mapStateToProps, mapDispatchToProps)(ProductList)