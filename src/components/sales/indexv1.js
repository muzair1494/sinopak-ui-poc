import React from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './salesList.css';
import { Title } from './../list/propslist';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles, Badge, Card, CardContent, CardActions, Grid, IconButton, TextField, MenuItem } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Button from '@material-ui/core/Button';
import ShoppingBasketRounded from '@material-ui/icons/ShoppingBasketRounded';
import Alert from '@material-ui/lab/Alert';

const ExpansionPanelSummaryStyle = withStyles({
    root: {
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        minHeight: '50px !important',
        '&$expanded': {
            minHeight: '50px !important',
        },
    }
})(MuiExpansionPanelSummary);
const ExpansionPanelSummaryStyleNested = withStyles({
    root: {
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        minHeight: '50px !important',
        backgroundColor: '#f9f9f9',
        '&$expanded': {
            minHeight: '50px !important',
        },
    }
})(MuiExpansionPanelSummary);
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: '#f9f9f9'
    }
}))(TableCell);
const StyledTableCellTD = withStyles((theme) => ({
    body: {
        padding: '10px 26px 10px 16px'
    }
}))(TableCell);
const StyledTableCellCart = withStyles((theme) => ({
    body: {
        padding: '5px 26px 5px 16px'
    }
}))(TableCell);
const useStyles = makeStyles((theme) => ({

    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));
const useStylesCartView = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    root2: {
        flexGrow: 1,
    },
});

function ProductsAccordian(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [nestedExpanded, setNestedExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    const nestedHandleChange = (panel) => (event, isExpanded) => {
        setNestedExpanded(isExpanded ? panel : false);
    };
    const products = props.allItems.filter((item) => item.product_category === props.item.product_category);
    console.log("Products ==> ", products)
    // const cartItems = props.cart.filter((cartItem) => cartItem.product_barcode === products.product_barcode);
    // console.log("cartItems ==> ", cartItems)
    return (
        <ExpansionPanel expanded={expanded === props.item.product_uuid} onChange={handleChange(props.item.product_uuid)}>
            <ExpansionPanelSummaryStyle
                expandIcon={<ExpandMoreIcon />}
                aria-controls={"panel-content" + props.keyIndex}
                id={"panel-header" + props.keyIndex}
            >
                <Typography className={classes.heading}>{props.item.product_category}</Typography>
                <Typography className={classes.secondaryHeading}>{props.item.product_barcode}</Typography>
            </ExpansionPanelSummaryStyle>
            <ExpansionPanelDetails>
                {/* Nested */}
                <div style={{ width: '100%' }}>
                    {products.map((item, index) => {
                        return (
                            <div style={{ width: '100%', margin: '10px 0' }}>
                                <ExpansionPanel key={item.uuid} expanded={nestedExpanded === item.uuid} onChange={nestedHandleChange(item.uuid)}>
                                    <ExpansionPanelSummaryStyleNested
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls={"panel-content" + item.uuid + "nested"}
                                        id={"panel-header" + item.uuid + "nested"}
                                    >
                                        <Typography className={classes.heading}>{item.product_description}</Typography>
                                        <Typography className={classes.secondaryHeading}>Product</Typography>
                                    </ExpansionPanelSummaryStyleNested>
                                    <ExpansionPanelDetails>
                                        <TableContainer component={Paper}>
                                            <Table aria-label="simple table" size="small">
                                                <TableHead>
                                                    <TableRow>

                                                        <StyledTableCell>UUID</StyledTableCell>
                                                        <StyledTableCell>Item</StyledTableCell>
                                                        <StyledTableCell>Barcode</StyledTableCell>
                                                        <StyledTableCell>Cost</StyledTableCell>
                                                        <StyledTableCell align="center">Action</StyledTableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {item.item.map((item, index) => {
                                                        const cart = props.cart.filter((cartItem) => cartItem.uuid === item.uuid);
                                                        return (
                                                            <TableRow key={item.uuid}>
                                                                <StyledTableCellTD>{item.uuid}</StyledTableCellTD>
                                                                <StyledTableCellTD>{item.product_item}</StyledTableCellTD>
                                                                <StyledTableCellTD>{item.product_item_barcode}</StyledTableCellTD>
                                                                <StyledTableCellTD>{item.product_item_cost}</StyledTableCellTD>
                                                                <StyledTableCellTD align="center">
                                                                    {cart.length > 0 ? <Button onClick={() => props.onRemoveCart(item)} variant="outlined" size="small" color="secondary">Remove</Button> : <Button onClick={() => props.onAddCart(item)} variant="outlined" size="small" color="primary">Add to Cart</Button>}
                                                                </StyledTableCellTD>
                                                            </TableRow>
                                                        );
                                                    })}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        );
                    })}
                </div>
                {/* Nested */}
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}

function CartView(props) {
    const classes = useStylesCartView();
    //const total = props.cartItems.reduce((totalCost, cost) => (totalCost + cost.product_item_cost), 0);
    //const additioncal = props.additional_cost;
    let total = props.additionalCost;
    console.log("addiontion_cost: ", total + " type: ", typeof (total))
    if (props.cartItems.length > 0) {
        return (
            <Card className={classes.root} variant="elevation">
                <CardContent>
                    <Grid container className={classes.root2} spacing={2}>
                        <Grid item xs={6}>
                            <Typography variant="subtitle1" gutterBottom>
                                Selected Items
                            </Typography>
                            <TableContainer component={Paper}>
                                <Table aria-label="simple table" size="small">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell>UUID</StyledTableCell>
                                            <StyledTableCell>Item</StyledTableCell>
                                            <StyledTableCell>Barcode</StyledTableCell>
                                            <StyledTableCell>Cost</StyledTableCell>
                                            <StyledTableCell align="center">Action</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {props.cartItems.map((item, index) => {
                                            total = total + Number(item.product_item_cost);
                                            //const cart = props.cart.filter((cartItem) => cartItem.uuid === item.uuid);
                                            console.log("Total Before Return : ", total)
                                            return (
                                                <TableRow key={item.uuid}>
                                                    <StyledTableCellCart>{item.uuid}</StyledTableCellCart>
                                                    <StyledTableCellCart>{item.product_item}</StyledTableCellCart>
                                                    <StyledTableCellCart>{item.product_item_barcode}</StyledTableCellCart>
                                                    <StyledTableCellCart>{item.product_item_cost}</StyledTableCellCart>
                                                    <StyledTableCellCart align="center">
                                                        <IconButton size="small" onClick={() => props.onRemoveCart(item)}>
                                                            <DeleteIcon color="secondary" />
                                                        </IconButton>
                                                    </StyledTableCellCart>
                                                </TableRow>
                                            );
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant="subtitle1" gutterBottom>
                                Items Details
                                <div style={{ width: '100%' }}>
                                    <div className="inputControl">
                                        <TextField
                                            id="outlined-select-currency"
                                            select
                                            label="Customer"
                                            onChange={(val) => props.onCustomerSelect(val.target.value)}
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                        >
                                            {props.customers.map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </div>
                                    <div className="inputControl">
                                        <TextField
                                            id="standard-multiline-static"
                                            label="Comments"
                                            multiline
                                            rows={2}
                                            placeholder="(Optional)"
                                            variant="outlined"
                                            fullWidth
                                        />
                                    </div>
                                    <div className="inputControlValue">
                                        <label>Total No. of items:</label>
                                        <span>{props.cartItems.length}</span>
                                    </div>
                                    <div className="inputControlValue">
                                        <Button size="" variant="outlined" color="primary">Upload Document</Button>
                                    </div>
                                    <div className="inputControlValue">
                                        <label>Total Cost:</label>
                                        <span>{total}</span>
                                    </div>
                                    <div className="inputControl">
                                        <TextField
                                            id="outlined-select-currency"
                                            select
                                            label="Add Cost"
                                            onChange={(val) => props.onCostTypeSelect(val.target.value)}
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                        >
                                            {props.costTypes.map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </div>
                                    {props.shipment_cost && <div className="inputControl display-flex">
                                        <TextField
                                            id="shipment-cost"
                                            label="Enter Shipment Cost"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={props.shipmentVal}
                                            onChange={(val) => props.setShipmentCost(val.target.value)}
                                        />
                                        <Button className="addCostBtn" onClick={() => props.addCost()}>Add Cost</Button>
                                    </div>}
                                    {props.tax_cost && <div className="inputControl display-flex">
                                        <TextField
                                            id="tax-cost"
                                            label="Enter Tax Cost"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={props.taxVal}
                                            onChange={(val) => props.setTaxCost(val.target.value)}
                                        />
                                        <Button className="addCostBtn" onClick={() => props.addCost()}>Add Cost</Button>
                                    </div>}
                                    {props.delivery_cost && <div className="inputControl display-flex">
                                        <TextField
                                            id="delivery-cost"
                                            label="Enter Delivery Cost"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={props.deliveryVal}
                                            onChange={(val) => props.setDeliveryCost(val.target.value)}
                                        />
                                        <Button className="addCostBtn" onClick={() => props.addCost()}>Add Cost</Button>
                                    </div>}
                                    {props.other_cost && <div className="inputControl display-flex">
                                        <TextField
                                            id="other-cost"
                                            label="Enter Other Cost"
                                            variant="outlined"
                                            fullWidth
                                            size="small"
                                            value={props.otherVal}
                                            onChange={(val) => props.setOtherCost(val.target.value)}
                                        />
                                        <Button className="addCostBtn" onClick={() => props.addCost()}>Add Cost</Button>
                                    </div>}
                                </div>
                                <div className="inputControl mt-30">
                                    <Button variant="contained" size="medium" color="primary">Submit</Button>
                                    <Button size="medium" onClick={() => props.onCancle()}>Cancle</Button>
                                </div>
                            </Typography>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>

                </CardActions>
            </Card>
        );
    } else {
        return (
            <div style={{ width: '400px' }}>
                <Alert severity="error" action={<Button color="inherit" size="small" onClick={() => props.onCancle()}>Go back</Button>}>Your cart is empty now.</Alert>
            </div>
        );
    }
}

class Sales extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            sortedlist: [],
            collectedCategory: [],
            cart: [],
            defaultView: true,
            cartView: false,
            loading: true,
            shipment_cost: false,
            other_cost: false,
            delivery_cost: false,
            tax_cost: false,
            shipment_cost_val: 0,
            tax_cost_val: 0,
            delivery_cost_val: 0,
            other_cost_val: 0,
            total: 0,
            gross_total: 0,
            additional_cost: 0,
        };
    }
    customerEnvoroment = [
        {
            value: "Test 1",
            label: "Test 1",
        },
        {
            value: "Test 2",
            label: "Test 2",
        },
        {
            value: "Test 3",
            label: "Test 3",
        },
    ];
    costTypeEnviroment = [
        {
            value: "Tax",
            label: "Tax",
        },
        {
            value: "Shipment Cost",
            label: "Shipment Cost",
        },
        {
            value: "Delivery Cost",
            label: "Delivery Cost",
        },
        {
            value: "Other Cost",
            label: "Other Cost",
        },
    ];
    componentDidMount() {
        axios.get('https://demo4681389.mockable.io/sales').then((response) => {
            const filteredResponse = response.data.Purchase_Orders.filter((data, ind) => data.type === 'stored');
            this.setState({ list: filteredResponse, loading: false });
            let setArr = [];
            for (var i = 0; i < filteredResponse.length; i++) {
                setArr.push(filteredResponse[i].product_category);
            }
            let set = new Set(setArr);
            var mySet = Array.from(set);
            const sortedList = filteredResponse.filter((item, index) => item.product_category === mySet[index]);
            this.setState({ sortedlist: sortedList, collectedCategory: mySet })
        }).catch(err => console.log('Error in Sales Api ==> ', err));

    }
    removeFromCart = (item) => {
        const cart = this.state.cart.filter((cartItem) => cartItem.uuid !== item.uuid);
        this.setState({ cart: cart });
        this.cartCalculation();
        if (cart.length === 0) {
            this.setState({ additional_cost: 0, gross_total: 0 });
        }
    };
    addToCart = (item) => {
        this.setState({ cart: this.state.cart.concat(item) });
    };
    onCartClick = () => {
        this.cartCalculation();
        this.setState({ defaultView: false, cartView: true })
    };
    onCancleClick = () => {
        this.setState({ defaultView: true, cartView: false })
    };
    handleCustomer = (val) => {
        console.log("Csstomer : ", val);
    };
    handleCostType = (val) => {
        console.log('Cost : ', val)
        if (val === "Tax") {
            this.setState({ shipment_cost: false, tax_cost: true, other_cost: false, delivery_cost: false });
        } else if (val === "Shipment Cost") {
            this.setState({ shipment_cost: true, tax_cost: false, other_cost: false, delivery_cost: false });
        } else if (val === "Delivery Cost") {
            this.setState({ shipment_cost: false, tax_cost: false, other_cost: false, delivery_cost: true });
        } else if (val === "Other Cost") {
            this.setState({ shipment_cost: false, tax_cost: false, other_cost: true, delivery_cost: false });
        } else {
            this.setState({ shipment_cost: false, tax_cost: false, other_cost: false, delivery_cost: false });
        }
        this.setState({ shipment_cost_val: 0, tax_cost_val: 0, delivery_cost_val: 0, other_cost_val: 0 })
    };
    cartCalculation = () => {
        const { cart } = this.state;
        // console.log('Addtition Cost ==> ', additional_cost);
        let total = 0;
        for (var i = 0; i < cart.length; i++) {
            total = total + Number(cart[i].product_item_cost);
        }
        this.setState({ total: total, gross_total: total });
    };
    setShipmentCost = (val) => {
        this.setState({ shipment_cost_val: val });
        // const { gross_total, shipment_cost_val } = this.state;
        // const shipment_add = gross_total + Number(shipment_cost_val);
        // this.setState({ gross_total: shipment_add });
    };
    setTaxCost = (val) => {
        this.setState({ tax_cost_val: val });
        // const { gross_total, tax_cost_val } = this.state;
        // const tax_add = gross_total + Number(tax_cost_val);
        // this.setState({ gross_total: tax_add });
    };
    setDeliveryCost = (val) => {
        this.setState({ delivery_cost_val: val });
        // const { gross_total, delivery_cost_val } = this.state;
        // const delivery_add = gross_total + Number(delivery_cost_val);
        // this.setState({ gross_total: delivery_add });
    };
    setOtherCost = (val) => {
        this.setState({ other_cost_val: val });
        // const { gross_total, other_cost_val } = this.state;
        // const others_add = gross_total + Number(other_cost_val);
        // this.setState({ gross_total: others_add });
    };
    addCost = () => {
        const { gross_total, shipment_cost_val, tax_cost_val, delivery_cost_val, other_cost_val } = this.state;
        const additional_cost = Number(shipment_cost_val) + Number(tax_cost_val) + Number(delivery_cost_val) + Number(other_cost_val);
        const cost = gross_total + Number(shipment_cost_val) + Number(tax_cost_val) + Number(delivery_cost_val) + Number(other_cost_val);
        this.setState({ gross_total: cost, additional_cost: additional_cost });
        console.log("cost : ", cost)
        console.log("gross total : ", gross_total)
        this.setState({ shipment_cost_val: 0, tax_cost_val: 0, delivery_cost_val: 0, other_cost_val: 0 });
    };
    render() {
        const { defaultView, cart, cartView, list, loading, shipment_cost, delivery_cost, tax_cost, other_cost, shipment_cost_val, tax_cost_val, delivery_cost_val, other_cost_val, gross_total, additional_cost } = this.state;
        return (
            <div>
                {defaultView && <div className="container-fluid">
                    <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                        <Title sty listTitle="Sales List" />
                    </div>
                    <div style={{ width: '100%' }}>
                        {this.state.sortedlist.map((item, index) => {
                            return (
                                <ProductsAccordian
                                    cart={cart}
                                    onRemoveCart={this.removeFromCart}
                                    onAddCart={this.addToCart}
                                    allItems={list}
                                    nesteduuid={item.uuid + "nested"}
                                    uniqueId={item.uuid + "asdasd"}
                                    key={index}
                                    keyIndex={index}
                                    item={item} />
                            );
                        })}
                        {loading && <div className="loadingTr">Loading ...</div>}
                    </div>
                    {cart.length > 0 && <div className="fixedBtnCover">
                        <Badge badgeContent={cart.length} color="secondary">
                            <Button variant="outlined" color="primary" className="fixedBtn" onClick={this.onCartClick}>
                                <ShoppingBasketRounded />
                            </Button>
                        </Badge>
                    </div>}
                </div>}
                {cartView && <div className="container-fluid">
                    <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                        <Title sty listTitle="Cart" />
                    </div>
                    <div style={{ width: '100%' }}>
                        <CartView
                            additionalCost={additional_cost}
                            addCost={this.addCost}
                            gross_total={gross_total}
                            setShipmentCost={this.setShipmentCost}
                            setTaxCost={this.setTaxCost}
                            setDeliveryCost={this.setDeliveryCost}
                            setOtherCost={this.setOtherCost}
                            shipmentVal={shipment_cost_val}
                            taxVal={tax_cost_val}
                            deliveryVal={delivery_cost_val}
                            otherVal={other_cost_val}
                            total={this.cartCalculation}
                            shipment_cost={shipment_cost}
                            tax_cost={tax_cost}
                            delivery_cost={delivery_cost}
                            other_cost={other_cost}
                            onCostTypeSelect={this.handleCostType}
                            costTypes={this.costTypeEnviroment}
                            customers={this.customerEnvoroment}
                            onCustomerSelect={this.handleCustomer}
                            cartItems={cart}
                            onCancle={this.onCancleClick}
                            onRemoveCart={this.removeFromCart} />
                    </div>
                </div>}
            </div>
        );
    }
}
export default Sales;