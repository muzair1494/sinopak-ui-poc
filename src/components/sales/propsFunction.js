import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {TwoItems} from './../../common/twoItemRow'
import FileUpload from './../../common/uploadFIle'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DropDown from './../../common/dropDownSelect'
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      // maxWidth: 1000,
      backgroundColor: theme.palette.background.paper,
    },
  }));

export const ItemList=({items,handleToggle,checked,selectId,change,editCost,editCost2,editCost3,editId,editId2,editId3,name,name2,setCost})=>{
    const classes = useStyles();
    return (
        <Paper elevation={3} style={{width:'100%',maxHeight:'500px',overflowY:'scroll'}} >
      <List dense className={classes.root}>
        {items.map((value,index) => {
          const labelId = `checkbox-list-secondary-label-${value.uuid}`;
          return (
            <ListItem key={value.uuid} button>
              {/* <ListItemAvatar>
                <Avatar
                  alt={`Avatar n°${index + 1}`}
                  src={`/static/images/avatar/${index + 1}.jpg`}
                />
              </ListItemAvatar> */}
              <ListItemText id={labelId} primary={value.item.name} />
              <ListItemText id={labelId} primary={value.item.barcode} />
              <ListItemText id={labelId} primary={value.item.vendor} />
              <ListItemText id={labelId} primary={<p><b>original sale price:</b>{value.cost.sale_price}</p>} />
              <ListItemText id={labelId} primary={ 
              <TextField id="standard-basic"
              onFocus={()=>selectId(value.uuid,value.cost.discount_price,'ship')}
              onChange={change}
              onBlur={()=>setCost(editCost,'ship')}
              name={name}
              label="discount sale price" value={value.uuid == editId ? editCost: value.cost.discount_price} />} />
              {/* <ListItemText id={labelId} primary={ 
              <TextField id="standard-basic"
              onFocus={()=>selectId(value.uuid,value.cost.othercost,'other')}
              onChange={change}
              onBlur={()=>setCost(editCost2,'other')}
              name={name2}
              label="other cost" value={value.uuid == editId2 ? editCost2: value.cost.othercost} />} /> */}
              
         <ListItemText id={labelId} primary={ 
            <TextField id="standard-basic"
            onFocus={()=>selectId(value.uuid,value.item.comment,'comment')}
            onChange={change}
            onBlur={()=>setCost(editCost3,'comment')}
            name="commentItem"
            label="comment" value={value.uuid == editId3 ? editCost3: value.item.comment} />} />
         <ListItemText id={labelId} primary={<FormControlLabel
            control={<Checkbox checked={value.promotion.applied}  />}
            label="Applied Promotion"
          /> } />
          <ListItemText id={labelId} primary={`Discount:${value.promotion.discount_number}${value.promotion.discount_type}`} />
              <ListItemSecondaryAction>
                <Checkbox
                  edge="end"
                  onChange={handleToggle(value.uuid)}
                  checked={checked.indexOf(value.uuid) !== -1}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
      </List>
      </Paper>
    );
}

export const ItemDetail =({sumOfDiscountCost,discount,disc_type,disc_types,handleChange2,checkeditm,purchaseCost,receivedItems,handleCapture,sumOfCost,change,comment,grn_no,shipment,tax,delivery,totalCost,username,entryDate,sysId,grn_date})=>{
    return <div key={totalCost} style={{width:'40%',maxHeight:'500px',overflowY:'scroll'}}> 
    <Paper  elevation={3} style={{width:'100%'}}>
        <TwoItems
             key={1}
             label="Comment:"
             content={<TextField label="comment" value={comment} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="comment"
                />}
            />
        <TwoItems
             key={2}
             label="SALE_NO:"
             content={<TextField label="grn_no" value={grn_no} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="grn_no."
                />}
            />
         <TwoItems
             key={3}
             label="Sale_Date:"
             content={<TextField type="date"  value={grn_date} style={{float:'right',width:'70%'}} id="standard-basic"
               onChange={change}
               name="grn_date"
                />}
            />
        <TwoItems
             key={4}
             label="Total Received Items:"
             content={getReceivedItems(receivedItems)}
            />
        <TwoItems
             key={5}
             label="Entry Date:"
             content={<p style={{float:'right'}}>{entryDate}</p>}
            />
        <TwoItems
             key={6}
             label="Customer Name:"
             content={<TextField value={username} label="customer" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="username"
                />}
            />
        <TwoItems
             key={7}
             label="System Id:"
             content={<p style={{float:'right'}}>{sysId}</p>}
            />
        <TwoItems
             key={8}
             label="Document:"
             content={<FileUpload 
                fl="right"
                change={handleCapture}
                />}
            />
        <TwoItems
             key={9}
             label="Shipment Cost:"
             content={<TextField value={shipment} label="shipment cost" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="shipment"
               onBlur={sumOfCost}
                />}
            />
        <TwoItems
             key={10}
             label="Tax_Rate:"
             content={<TextField value={tax} label="tax" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="tax"
               onBlur={sumOfCost}
                />}
            />
        <TwoItems
             key={11}
             label="Delivery Cost:"
             content={<TextField value={delivery} label="delivery cost" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="delivery"
               onBlur={sumOfCost}
                />}
            />
        <TwoItems
             key={12}
             label="Total purchase cost:"
             content={<p>{purchaseCost}</p>}
            />
        
        <FormControlLabel
            control={<Checkbox checked={checkeditm} onChange={handleChange2} name="checkeditm" />}
            label="Applied Promotion"
          />

        

            {checkeditm && <TwoItems
             key={15}
             label="Discount Type:"
             content={<DropDown 
                name="disc_type"
                value={disc_type}
                change={change}
                title="Discount type"
                >
                    {disc_types.map(item=><MenuItem  value={item}>{item}</MenuItem>)}
                </DropDown>}
            />}

{checkeditm && <TwoItems
            key={14}
            label="Discount:"
            content={<TextField value={discount} label="discount" style={{float:'right'}} id="standard-basic"
            onChange={change}
            name="discount"
            onBlur={sumOfDiscountCost}
                />}
            />}
        <TwoItems
             key={13}
             label="Total Sale Price:"
             content={<p>{totalCost}</p>}
            />        
    </Paper>
    </div>
}

const getReceivedItems = (rows)=>{
    return  <TableContainer component={Paper}>
    <Table  size="small" aria-label="a dense table">
      <TableHead>
        <TableRow>
          <TableCell>Product</TableCell>
          <TableCell align="right">Barcode</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow key={row.item.namee}>
            <TableCell component="th" scope="row">
              {row.item.name}
            </TableCell>
            <TableCell align="right">{row.item.barcode}</TableCell>
          </TableRow>
        ))}
        <TableRow key={1}>
            <TableCell component="th" scope="row">
              total:
            </TableCell>
            <TableCell align="right">{rows.length}</TableCell>
          </TableRow>
      </TableBody>
    </Table>
  </TableContainer>
}

export const ButtonComponent = ({click,name}) =>{
    return <Button onClick={click} variant="contained" color="primary">
               {name} 
           </Button>
}