import React,{Component} from 'react'
import {ItemList,ButtonComponent, ItemDetail} from './propsFunction'
import { connect } from 'react-redux';
import { getGrnList } from './../../actions/simpleAction'
import FontAwesome from 'react-fontawesome'

const disc_types = ["%","no."]
class GRN extends Component{

    constructor(props){
        super(props)
        this.state={
            items:[1,2,3,4,5,6],
            checked:[],
            pending:true,
            grn:[],
            selectId:'',
            selectId2:'',
            selectId3:'',
            shipmentCost:0,
            other:0,
            commentItem:'',
            search:'',
            openForm:false,
            comment:"",
            grn_no:"",
            shipment:0,
            tax:0,
            delivery:0,
            totalCost:0,
            username:"",
            entryDate:"",
            grn_date:"",
            sysId:"abcd1#2345",
            itemsCost:0,
            filteredItems:[],
            checkeditm:false,
            disc_type:'',
            discount:0
        }
    }
    componentDidMount(){
        this.props.getGrnList()
        this.getCurrentDateTime()
    }
    componentWillReceiveProps(nextprops){
      if(this.props.grn != nextprops.grn){
        this.setState({pending:nextprops.pending})
        nextprops.grn.map(item=>{
          return item.cost.discount_price = 0
        })
        let arr=[]
        for(var i=0;i<nextprops.grn.length;i++){
          if(nextprops.grn[i].type == 'stored'){ 
          arr.push(nextprops.grn[i])
        }
        }
      this.setState({grn:arr},()=>{
        this.setState({
          grn: this.state.grn.map((product)=>{
            if(product.promotion.applied && product.promotion.discount_type == "%"){
              let discount_number = Number(product.promotion.discount_number)
              discount_number = discount_number/100
              let discount_amount = discount_number * Number(product.cost.sale_price)
              product.cost.discount_price = product.cost.sale_price - discount_amount
            }else{
              product.cost.discount_price = product.cost.sale_price
            }
            return product
          })
        })
      })
     }
    }
    
    getCurrentDateTime=()=>{
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        this.setState({entryDate:dateTime})
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]: e.target.value,open:false})
    }
    handleChange2=(e)=>{
        this.setState({[e.target.name]: e.target.checked,open:false})
    }
    handleToggle = (value) => () => {
        const {checked} = this.state
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }
        this.setState({checked:newChecked},()=>console.log(this.state.checked));
      };
      selectId=(id,cost,key)=>{
        if (key== 'ship') {this.setState({selectId:id,shipmentCost:cost})}
        if (key == "other") {this.setState({selectId2:id,other:cost})}
        if (key == "comment") {this.setState({selectId3:id,commentItem:cost})}
      }
      setCost=(cost,key)=>{
        if (key == "ship"){
        this.setState({
            grn: this.state.grn.map((item)=>{
                if(item.uuid === this.state.selectId){
                  item.cost.discount_price = cost
                }
                return item
            })  
          })}
          if (key == "other"){
            this.setState({
                grn: this.state.grn.map((item)=>{
                    if(item.uuid === this.state.selectId2){
                      item.cost.othercost = cost
                    }
                    return item
                })  
              })  
          }
          if (key == "comment"){
            this.setState({
                grn: this.state.grn.map((items)=>{
                    if(items.uuid === this.state.selectId3){
                      items.item.comment = cost
                    }
                    return items
                })  
              })  
          }
      }
      selectedItems=()=>{
        this.setState({itemsCost:0,filteredItems:[]},()=>{
          const {checked,grn} = this.state
          let filteredItems = grn.filter((item)=>{
              return checked.includes(item.uuid)== true ? item : null
            })
            this.setState({openForm:true,filteredItems},()=>{
                let itemsCost = filteredItems.reduce(function(sum, number){
                  // no need for += anymore due to sum
                  return sum=sum + Number(number.cost.discount_price)
                }, 0)
                this.setState({itemsCost},()=>this.sumOfCost())
            })

        })
      }
      sumOfCost=()=>{
        const { shipment,tax, delivery, itemsCost } = this.state
        this.setState({totalCost:Number(shipment)+Number(tax)+Number(delivery)+Number(itemsCost)})
      }
      sumOfDiscountCost=()=>{
        const { totalCost, shipment,tax, delivery, itemsCost, discount } = this.state
        this.setState({totalCost:Number(shipment)+Number(tax)+Number(delivery)+Number(itemsCost)},()=>{
          let disc = Number(discount)/100
          disc = Number(totalCost) * disc
          let finaldata = Number(totalCost) - disc
          this.setState({totalCost:finaldata})
        })
      }
      handleCapture = (e ) => {
        const fileReader = new FileReader();
        const files = e.target.files
        console.log(files,'files')
        if(files[0].type == "application/pdf"){
        fileReader.readAsDataURL(files[0]);
        fileReader.onload = (e) => {
            //const apiURL = "http//..."
            //const formatFileData = {file: e.target.result}
            //at the top import libraray
            //import axios ,{post} from 'axios'
            //return post(apiURL,formatFileData)
            //.then(res=>..)
            // console.log(e.target.result,'target.accept')
        }
        // fileReader.onload = (e) => {
        //     this.setState((prevState) => ({
        //         [name]: [...prevState[name], e.target.result]
        //     }));
        // };
    }else{alert('please insert only pdf file')}
    };
    dispComp =()=>{
        this.setState({openForm:false})
    }
    postData=()=>{
        const {comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId} = this.state
        const values = {comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId}
        console.log(values,'values')
    }
    render(){
        const {pending,discount,disc_type,itemsCost,selectId2,selectId3,filteredItems,grn,shipmentCost,other,commentItem,selectId,search,openForm} = this.state
        const {checked,checkeditm,comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId
        } = this.state
        let filteredData = grn.filter(items => {
            return (items.item.name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1);
        })
        return(<div >
                {pending && <h3>Loading...</h3>}
                {!openForm && !pending && <div style={{display:'flex',justifyContent:'center',marginTop:'10px'}}>
                <ItemList 
                selectId={this.selectId}
                checked={checked} handleToggle={this.handleToggle}
                change={this.handleChange}
                editCost={shipmentCost}
                editCost2={other}
                editCost3={commentItem}
                name="shipmentCost"
                name2="other"
                editId={selectId}
                editId2={selectId2}
                editId3={selectId3}
                items={filteredData}
                setCost={this.setCost}
                />
            </div>}
            {!openForm && !pending && <div style={{marginTop:'5px'}}>
                <ButtonComponent
                click={this.selectedItems}
                name="Submit"
                />
            </div>}
            {openForm && <div style={{display:'flex',justifyContent:'center',width:'64%',marginTop:'5px'}}>
              <FontAwesome
              className="super-crazy-colors"
              name="arrow-left"
              onClick={this.dispComp}
              size="2x"
              // spin
              style={{marginTop:'17px',color:"blue" ,textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
              />
              <h2 style={{marginLeft:'10px'}}>Detail</h2>
            </div>}
            {openForm && <div style={{display:'flex',justifyContent:'center',marginTop:'10px'}}>
                <ItemDetail
                comment={comment}
                grn_no={grn_no}
                shipment={shipment}
                tax={tax}
                delivery={delivery}
                totalCost={totalCost}
                username={username}
                entryDate={entryDate}
                sysId={sysId}
                grn_date={grn_date}
                change={this.handleChange}
                sumOfCost={this.sumOfCost}
                handleCapture={this.handleCapture}
                receivedItems={filteredItems}
                purchaseCost={itemsCost}
                checkeditm={checkeditm}
                handleChange2={this.handleChange2}
                disc_type={disc_type}
                disc_types={disc_types}
                discount={discount}
                sumOfDiscountCost={this.sumOfDiscountCost}
                />
            </div> }
            {openForm && <div style={{display:'flex',justifyContent:'center',width:'68%',marginTop:'5px'}}>
                <ButtonComponent
                click={this.postData}
                name="Confirm"
                />
            </div>}
        </div>)
    }
}


const mapDispatchToProps = dispatch => ({
    getGrnList: () => dispatch(getGrnList())
   })
const mapStateToProps = state => ({
    grn: state.simpleReducer.GRN_List,
    pending: state.simpleReducer.pending_GRN,
   })

export default connect(mapStateToProps, mapDispatchToProps)(GRN)