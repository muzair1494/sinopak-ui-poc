import React, {Component} from 'react'
import axios from 'axios';
import {SinoInventory} from './propsFunction'
import {Title} from './../list/propslist' 

export default class Product extends Component{
    constructor(props){
        super(props);
        this.state={
            products:[]
        }
    }
    
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/sino_inventory').then((res) => {
            this.setState({ products: res.data.invemntory, loading: false })
            console.log(this.state.products)
        })
    }
render(){
    let {products} = this.state
    return(<div>
        <Title listTitle="Inventory List" />
        <SinoInventory sinoInventory={products}/>
    </div>)
}
}