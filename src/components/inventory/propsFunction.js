import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


export const SinoInventory =({sinoInventory})=>{return <TableContainer component={Paper}>
<Table  aria-label="simple table">
    <TableHead>
        <TableRow>
            <TableCell>uuid</TableCell>
            <TableCell>Qty</TableCell>
            <TableCell>Cost</TableCell>
        </TableRow>
    </TableHead>
    <TableBody>
        {sinoInventory.map((item) => (
            <TableRow key={item.uuid}>
                <TableCell component="th" scope="row">{item.uuid}</TableCell>
                <TableCell>{item.qty}</TableCell>
                <TableCell>{item.cost}</TableCell>
            </TableRow>
        ))}
    </TableBody>
</Table>
</TableContainer>}

