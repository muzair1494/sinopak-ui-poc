import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Products } from '../GRN/propsFunction';

import FontAwesome from 'react-fontawesome'
import TreeView from '@material-ui/lab/TreeView';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';


const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    //   height: 400,
      maxWidth: 600,
      backgroundColor: theme.palette.background.paper,
      padding:0,
      margin:0
    },
    detailroot: {
      width: '100%',
      maxWidth: '36ch',
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: 'inline',
    },
}));
  

export const Div =(props)=>{
return <div style={{display:'block'}}>
            <div style={{padding:'20px',display:'inline-flex'}}>
                    <p style={{marginRight:'10px'}}>{props.title}</p>
                    {props.children}
            </div>
        </div>
}

export const  VirtualizedList=({descName,openDescClick,openDesc,myCategory,products,productsv1,getOneProduct,name}) =>{
    const classes = useStyles();
    console.log(productsv1,'productsv1')
    return (
        <div className={classes.root}>
          
          {name !== "selectedProd" && myCategory.map(chekCat=><div  key={chekCat}>
          <Paper style={{background:descName === chekCat ?'#9BB1B4' : '#C3D8DB' ,margin:'5px',padding:'5px'}} onClick={()=>openDescClick(chekCat)}>
          <Grid  container spacing={2}>
                    <Grid item xs={6}>{chekCat}</Grid>
                    <Grid item xs={6}>
          {descName !== chekCat ? <FontAwesome
                        className="super-crazy-colors"
                        name="arrow-up"
                        // cssModule={faStyles}
                        size="1x"
                        // spin
                        style={{marginTop:'10px',float:"right",color:"grey"}}
                        />:
                        <FontAwesome
                        className="super-crazy-colors"
                        name="arrow-down"
                        // cssModule={faStyles}
                        size="1x"
                        // spin
                        style={{marginTop:'10px',float:"right",color:"grey" }}
                        />
                        }
                        </Grid>
                        </Grid>
                        </Paper>
         {descName == chekCat && name !== "selectedProd" && <ListItem  key={1}>
            {/* <ListItemText  primary="Category" /> */}
            {/* <ListItemText  primary={<h3>Description</h3>} /> */}
            <h3>Description</h3>
            {/* <ListItemText  primary="View Products" /> */}
          </ListItem>}
          {descName == chekCat && name !== "selectedProd" && productsv1.map(productsv1=> productsv1.category === chekCat && <ExpansionPanel style={{width:'100%'}} >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <ListItem  key={1}>
          {/* <ListItemText  primary={productsv1.category} /> */}
          <ListItemText  primary={productsv1.description}/>
          {/* <ListItemText  primary="Products" /> */}
          </ListItem>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
         <div style={{width:'100%'}}> <Paper elevation={3} >
         <ListItem style={{background:'#318C9B',marginTop:'2px'}}  key={1}>
          <ListItemText  primary="Image" />
          <ListItemText  primary="Name" />
          <ListItemText  primary="Barcode" />
          {/* <ListItemText  primary="Weight" /> */}
        </ListItem>
        </Paper>
        {productsv1.items.map(item=> <Paper style={{width:'100%'}} elevation={3} key={item.name}>
         <ListItem onClick={()=>getOneProduct(item,name,productsv1.category,productsv1.description)} style={{background:'rgb(215, 222, 221)',marginTop:'2px'}} button key={item.name}>
         <ListItemText  secondary={item.image} />
          <ListItemText  secondary={item.name} />
         <ListItemText  secondary={item.barcode} />
        {/* <ListItemText  secondary={item.weight} /> */}
        </ListItem>
    </Paper>
    )}
           </div>
</ExpansionPanelDetails>
</ExpansionPanel>)}

</div>)}
      {name === "selectedProd" && <ListItem style={{background:'#318C9B',marginTop:'2px'}} button key={23}>
         <ListItemText style={{margin:'10px'}} secondary="Name" />
          {/* <ListItemText style={{margin:'10px'}} secondary="Category" /> */}
          {/* <ListItemText style={{margin:'10px'}} secondary="Description" /> */}
          <ListItemText style={{margin:'10px'}} secondary="Barcode" />
          {/* <ListItemText style={{margin:'10px'}} secondary="Weight" /> */}
          <ListItemText style={{margin:'10px'}} secondary="Qty" />
          <ListItemText style={{margin:'10px'}} secondary="Unit Cost" />
        </ListItem>}
      {name === "selectedProd" && productsv1.map(item=> <Paper  elevation={3} key={item.name}>
         <ListItem onClick={()=>getOneProduct(item,name)} style={{background:'rgb(215, 222, 221)',marginTop:'2px'}} button key={item.name}>
          <ListItemText style={{margin:'10px'}} secondary={item.name} />
          {/* <ListItemText style={{margin:'10px'}} secondary={item.category} /> */}
          {/* <ListItemText style={{margin:'10px'}} secondary={item.description} /> */}
          <ListItemText style={{margin:'10px'}} secondary={item.barcode} />
          {/* <ListItemText style={{margin:'10px'}} secondary={item.weight} /> */}
          <ListItemText style={{margin:'10px'}} secondary={item.count} />
          <ListItemText style={{margin:'10px'}} secondary={item.totalCost} />
        </ListItem>
    </Paper>
    )}
    </div>
);
}


export const DetailProduct =({itemsOrderv1,itemsOrder,Iqty,Icost,Icomment,getItems,change,clearItems})=>{
  return  <Grid container spacing={0}>
        <Grid item xs={4}><h4>{itemsOrderv1.image}</h4></Grid>
        <Grid item xs={8}>
          <span style={{display:'block',padding:'5px'}}>Name: {itemsOrderv1.name}</span>
          <span style={{display:'block',padding:'5px'}}>Barcode: {itemsOrderv1.barcode}</span>
          <span style={{display:'block',padding:'5px'}}>Weight: {itemsOrderv1.weight}</span>
        </Grid>
        <ListItem   key={1}>
          <ListItemText style={{width:"20%"}} secondary='Quantity' />
          <ListItemText  secondary={ <TextField onChange={change} name="Iqty" id="standard-basic" label="qty" value={Iqty} />} />  
        </ListItem>
        <ListItem   key={2}>
          <ListItemText style={{width:"20%"}}  secondary='All Items  PurchaseCost' />
          <ListItemText  secondary={ <TextField  onChange={change} name="Icost" id="standard-basic" label="cost" value={Icost} />} />  
        </ListItem>
        <ListItem   key={3}>
          <ListItemText style={{width:"20%"}}  secondary='Comment' />
          <ListItemText  secondary={ <TextField onChange={change} name="Icomment" id="standard-basic" label="comment" value={Icomment} />} />  
        </ListItem>
        <Button style={{float:'right'}}
        onClick={()=>getItems(itemsOrderv1,Iqty,Icost,Icomment)}
        variant="contained" color="">
          Add
        </Button>
        <Button onClick={()=>clearItems(itemsOrder)} style={{float:'right',marginLeft:'5px'}} variant="contained" color="secondary">
          Clear
        </Button>
      </Grid>
}


export const PaperWithScroll = (props)=>{
  return  <Paper
    style={{height:'427px',overflowY:'scroll'}}
    elevation={3} >
    {props.children}
  </Paper>
}

export const TwoItemsWith5Width =(props)=>{
  return <ListItem  key={props.key}>
      <ListItemText style={{width:"5%"}}  primary={props.label} />
      <ListItemText  secondary={props.content} />
  </ListItem>
}