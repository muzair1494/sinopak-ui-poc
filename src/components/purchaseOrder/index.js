import React , {Component} from 'react'
import { connect } from 'react-redux';
import { getPurchasedProducts, getProducts } from './../../actions/simpleAction'
import {Description} from './../../common/descriptionField'
import ContainerPapaer from './../../common/conatiner'
import BorderedInputField from './../../common/borderedInputField'
import {VirtualizedList, DetailProduct, PaperWithScroll, TwoItemsWith5Width} from './propsFunction'
import FileUpload from './../../common/uploadFIle'
import DropDown from './../../common/dropDownSelect'
import {GetSearchData} from './../../common/searchINput'
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button'
import Badge from '@material-ui/core/Badge';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import {TwoItems} from './../../common/twoItemRow'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FontAwesome from 'react-fontawesome'
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const vend = ["vendor-1","vendor-2","vendor-3","vendor-4"]

class Purchase extends Component{
    constructor(props){
        super(props)
        this.state={
            products:[],
            pending:true,
            temp:[],
            temp2:[],
            itemsOrder:{},
            Iqty:0,
            Icost:0,
            Icomment:'',
            submitItem:[],
            search:'',
            ID: 'd83972ac-75b8-11ea-bc55-0242ac130003',
            OrderId:'df48c0b6-75b8-11ea-bc55-0242ac130003',
            purchaseDate:'Wed Mar 25 2015 05:00:00 GMT+0500',
            poid:'',
            desc:'',
            ctdterms:'',
            openForm:false,
            vendor:'',
            dispDetail:false,
            addedItemCount:0,
            grandItem:0,
            storedValues:[],
            panel:-1,
            purchaseValue:0,
            commentValue:'',
            adv_pay:'',



            pendingv1:false,
            tempv1:[],
            productsv1:[],
            itemsOrderv1:{},
            cat:'',
            desc:'',
            uid:'',
            chek:false,
            sumCosts:0,
            myCategory:[],
            openDesc:false,
            descName:''
            
        }
    }
    componentDidMount(){
        this.props.getPurchasedProducts()
        this.props.getProducts()
    }
   
    static getDerivedStateFromProps(props, state) {
        if (props.pending !== state.pending) {
            return {
                 pending: props.pending
           };
         }
        if (props.products !== state.temp) {
           return {
                temp: props.products
          };
        }
    
        if (props.pendingv1 !== state.pendingv1) {
            return {
                 pendingv1: props.pendingv1
           };
         }
        if (props.productsv1 !== state.tempv1) {
           console.log(props.productsv1)
            return {
                tempv1: props.productsv1
          };
        }
        // Return null if the state hasn't changed
        return null;
      }
    pushValues=(products)=>{
        products.map((item)=>{
            item.totalCost = 0
            item.count = item.items.length
            item.comment=""
            return item
        })
    }
    pushValuesv1=(products)=>{
        products != undefined && products.map((item)=>{
            item.totalCost = 0
            item.count = item.items.length
            item.comment=""
            item.category=""
            item.description = ""
            return item
        })
    }
    componentDidUpdate(prevProps, prevState) {
        // if (this.props.products !== prevProps.products) {
        //     this.getProdSuccess();
        // }
        if (this.props.productsv1 !== prevProps.productsv1) {
            this.getProdSuccessv1();
        }
    }
    getProdSuccess=()=>{
        this.pushValues(this.state.temp)
        this.setState({temp: this.state.temp.map((item)=>{
            item.items.map(child=> item.totalCost += child.cumulativeCost)   
            return item
        })
        },()=>this.setState({products:this.state.temp},()=>console.log(this.state.products,'productsv0')))
    }
    getProdSuccessv1=()=>{
        this.pushValuesv1(this.state.tempv1)
        this.setState({tempv1: this.state.tempv1.map((item)=>{
            item.items.map(child=> item.totalCost += child.cumulativeCost)   
            return item
        })
        },()=>this.setState({productsv1:this.state.tempv1},()=>{
            const {productsv1} = this.state
            let setArr=[]
            for(var i=0 ;i< productsv1.length;i++){
                setArr.push(productsv1[i].category)
            }
            let set = new Set(setArr)
            var mySet = Array.from(set)
            this.setState({myCategory:mySet})
        }))
    }

    handleChange=(e)=>{
        this.setState({[e.target.name]: e.target.value,open:false})
    }
    getProductItems=(product,key,cat,desc)=>{
        console.log(product)
        if(key == 'unselectedProd'){
        this.setState({dispDetail:true,chek:true,cat,desc,uid:product.uuid,itemsOrder:product,Iqty:0,Icost:0,Icomment:''})
        this.setState({dispDetail:true,itemsOrderv1:product,Iqty:0,Icost:0,Icomment:''})}
        else{   
            this.setState({storedValues:this.state.storedValues,grandItem:product['uuid']},()=>{
                console.log(this.state.storedValues,'cehkingsss')
            })
        }
    }
    getItems=(itemsOrderv1,Iqty,Icost,Icomment)=>{
        console.log(itemsOrderv1,'itemsOrder')
        this.setState({submitItem:[...this.state.submitItem,itemsOrderv1]},()=>{
            console.log(this.state.submitItem,'called')
            this.setState({submitItem:this.state.submitItem.map((item,index)=>{
                if(this.state.uid == item.uuid){
                item.count = Iqty
                item.totalCost = Icost
                item.comment = Icomment
                item.category = this.state.cat
                item.description =  this.state.desc}
                return item
            })},()=>{
            var arr=[]
            let storedValues={}
            for(var i=0;i<Iqty ;i++){
                let values ={
                    id:i,
                    purchaseCost: itemsOrderv1.totalCost,
                    comment: itemsOrderv1.comment,
                
                }
                arr.push(values)
            }
            storedValues = {
                product_id: itemsOrderv1['uuid'],
                product_name: itemsOrderv1.name,
                itemList: arr
            }
            this.setState({storedValues:[...this.state.storedValues,storedValues]},()=>console.log(this.state.storedValues,'subItems'))
            })
            this.setState({addedItemCount:this.state.chek == true  ? this.state.addedItemCount+1 : this.state.addedItemCount},()=>{
                this.setState({chek:false})
            })
        })
    }
    clearItems=(items)=>{
        this.setState({addedItemCount:this.state.addedItemCount == 0 ? 0 : this.state.addedItemCount-1,Iqty:0,Icost:0,Icomment:''})
    }
    submit=()=>{
        this.setState({openForm:!this.state.openForm,search:''})
        var sums=0 
            let sumUP = this.state.storedValues.reduce((x,item)=>{
            item.itemList.map(child=>{
              sums = sums  + Number(child.purchaseCost)
            })
            return sums
        },0)
          this.setState({sumCosts: sumUP})
    }
    handleCapture = (e ) => {
        const fileReader = new FileReader();
        const files = e.target.files
        console.log(files,'files')
        if(files[0].type == "application/pdf"){
        fileReader.readAsDataURL(files[0]);
        fileReader.onload = (e) => {
            //const apiURL = "http//..."
            //const formatFileData = {file: e.target.result}
            //at the top import libraray
            //import axios ,{post} from 'axios'
            //return post(apiURL,formatFileData)
            //.then(res=>..)
            // console.log(e.target.result,'target.accept')
        }
        // fileReader.onload = (e) => {
        //     this.setState((prevState) => ({
        //         [name]: [...prevState[name], e.target.result]
        //     }));
        // };
    }else{alert('please insert only pdf file')}
    };
    openPanel=(panel)=>{
        this.setState({panel:panel.id,purchaseValue:panel.purchaseCost,
                        commentValue:panel.comment})
    }
    getValueChanged=(index,itemss,itemChildId,key)=>{
        this.setState({
            temp2: this.state.storedValues[index].itemList.map((item)=>{
                if(item.id === itemChildId){
                    if(key === 'p'){item.purchaseCost = this.state.purchaseValue}
                    if(key === 'c'){item.comment = this.state.commentValue}
                }
                return item
            })
        },()=>this.setState({
            storedValues:this.state.storedValues.map(item=>{
                if(item.product_id == itemss.product_id){
                    item.itemList = this.state.temp2
                }
                return item
            })
        },()=>{
            var sums=0 
            let sumUP = this.state.storedValues.reduce((x,item)=>{
            item.itemList.map(child=>{
              sums = sums  + Number(child.purchaseCost)
            })
            return sums
        },0)
          this.setState({sumCosts: sumUP})
        }))
    }
    getSearchedData=(val,key)=>{
        console.log(key)
        if(key == 'PRO'){
            // let arr=[]
            // for(var i=0 ;i<this.state.productsv1.length; i++){
                // let finding = this.state.productsv1[0].items.find(item=>item.uuid == '12ab')
                // console.log(finding,'findingnemo')
                
                // let values ={
                //     uuid : this.state.productsv1[i].uuid,
                //     type : this.state.productsv1[i].type,        
                //     purchase_order_no: this.state.productsv1[i].purchase_order_no,
                //     vendor_name: this.state.productsv1[i].vendor_name,
                //     vendor_address: this.state.productsv1[i].vendor_address,
                //     vendor_contact: this.state.productsv1[i].vendor_contact,
                //     vendor_email: this.state.productsv1[i].vendor_email,
                //     ordered_date: this.state.productsv1[i].ordered_date,
                //     credit_term: this.state.productsv1[i].credit_term,
                //     payment_advance: this.state.productsv1[i].payment_advance,
                   
                //             }
                //     arr.push(values)
            // }
            // console.log(arr)
            // this.setState({productsv1:arr},()=>{
            //     console.log(this.state.productsv1,arr)
            // })
        }
    }
    openDescClick=(name)=>{
        this.setState({openDesc:true,descName:name})
    }
    render(){
        const {descName,openDesc,myCategory,sumCosts,itemsOrderv1,productsv1,adv_pay,ctdterms,purchaseValue,commentValue,grandItem,storedValues,addedItemCount,dispDetail,submitItem,vendor,openForm,desc,poid,ID,OrderId,purchaseDate,Icomment,Iqty,Icost,products,pending,itemsOrder,search}= this.state
        let filteredData = productsv1.filter(items => {
            return (items.category.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1
            || items.description.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1
            );
        })
        let filteredSubmitData = submitItem.filter(items => {
            return (items.name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1);
        })
        
        return(<div >
      {!openForm &&  <ContainerPapaer>
        <h1>Purchase Order</h1>
        <Divider />
        {pending && <h4>loading....</h4>}
        <Grid  container spacing={0}>
            {!pending && <Grid item xs={6}>
            <PaperWithScroll>
                <Grid  container spacing={0}>
                    <Grid item xs={1}><p style={{paddingTop:'10px'}}>Product:</p></Grid>
                    <Grid item xs={1}></Grid>
                    <Grid item xs={8}>
                        <GetSearchData 
                        val="PRO"
                        onBlur={this.getSearchedData}
                        name="search"
                        search={search} 
                        getSearched={this.handleChange}
                        title="Products Search" />
                    </Grid>
                    <Grid item xs={2}><FontAwesome
                        className="super-crazy-colors"
                        name="plus-circle"
                        // cssModule={faStyles}
                        size="3x"
                        // spin
                        style={{marginTop:'10px',color:"blue" ,textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                        />
                    </Grid>
                </Grid>
                <VirtualizedList 
                descName={descName}
                openDescClick={this.openDescClick}
                openDesc={openDesc}
                getOneProduct={this.getProductItems}
                productsv1={filteredData}
                products={filteredData}
                myCategory={myCategory}
                name='unselectedProd'
                />
            </PaperWithScroll>
            </Grid>}
            <Grid item xs={6}>
                {dispDetail && <Paper
                    style={{padding:'10px'}}
                    elevation={3} >
                    <h2>Detail</h2>
                    <DetailProduct
                    Iqty={Iqty}
                    Icost={Icost}
                    Icomment={Icomment}
                    getItems={this.getItems}
                    itemsOrder={itemsOrder}
                    itemsOrderv1={itemsOrderv1}
                    change={this.handleChange}
                    clearItems={this.clearItems}
                    />
                </Paper>}
            </Grid>
        </Grid>
        <p style={{marginTop:'10px'}}></p>
        <Divider />
        <p style={{marginTop:'10px'}}></p>
        <Badge badgeContent={addedItemCount}   color="secondary">
            <Button onClick={this.submit} disabled={addedItemCount>0 ? false:true} variant="contained" color="primary">
                View Order
            </Button>
        </Badge> 
        <Button onClick={this.cancelSubmit} style={{marginLeft:'5px'}} variant="contained" color="">
            cancel
        </Button>
    </ContainerPapaer>}
    {openForm && <ContainerPapaer>
        <h1>Purchse Order</h1>
        <Divider />
        {/* <TwoItems
        key={1}
        label="Id:"
        content={ID}
        /> */}
        <TwoItems
        key={2}
        label="Date:"
        content={purchaseDate}
        />
        <TwoItemsWith5Width 
        key={3}
        label="PO NO. :"
        content={<BorderedInputField 
            name="poid"
            lable="POID"
            change={this.handleChange}
            />}
        />        
        <TwoItemsWith5Width 
        key={4}
        label="Description:"
        content={<Description 
            name="desc"
            lable="description"
            change={this.handleChange}
            />}
        />
        <TwoItemsWith5Width
        key={14}
        label="Advance payment in %"
        content={<TextField id="standard-basic" label="Advance Payment"
        name="adv_pay" value={adv_pay}  onChange={this.handleChange}
        />}
        />
        <TwoItemsWith5Width 
        key={5}
        label="Document Upload:"
        content={
        <FileUpload 
        fl="left"
        change={this.handleCapture}
        />}
        />
        <TwoItemsWith5Width 
        key={17}
        label="Credit Terms:"
        content={
            <DropDown
            floating="left" 
            name="ctdterms"
            value={ctdterms}
            change={this.handleChange}
            title="Credit Terms"
            >
                <MenuItem value="Net 30 days">Net 30 days</MenuItem>
                <MenuItem value="Net 40 days">Net 40 days</MenuItem>
                <MenuItem value="Net 50 days">Net 50 days</MenuItem>
            </DropDown>}
        />
        <TwoItemsWith5Width 
        key={6}
        label="Vendor:"
        content={
            <DropDown
            floating="left" 
            name="vendor"
            value={vendor}
            change={this.handleChange}
            title="Vendor"
            >
                {vend.map(item=><MenuItem value={item}>{item}</MenuItem>)}
            </DropDown>}
        />
        {pending && <h4>loading....</h4>}
        <Grid  container spacing={0}>
            {!pending && <Grid item xs={6}>
                <h3>Basket</h3>
                <PaperWithScroll>
                    <ListItem   key={1}>
                        <ListItemText style={{width:"5%"}} primary='Item:' />
                        <ListItemText  secondary={<GetSearchData 
                        val="COST"
                        onBlur={this.getSearchedData}
                        name="search"
                            search={search} 
                            getSearched={this.handleChange}
                            title="Item Search" />} 
                        />  
                    </ListItem>
                    <VirtualizedList 
                    getOneProduct={this.getProductItems}
                    products={filteredSubmitData}
                    productsv1={filteredSubmitData}
                    name='selectedProd'
                    />
                   
                </PaperWithScroll>
                <p>Total Cost: {sumCosts}</p>
            </Grid>}
            <Grid item xs={6}>
            <h3>Basket-Item Detail</h3>
                <PaperWithScroll>
                    <h2>Items</h2>
                    {storedValues.map((item,index)=>{
                        return grandItem == item.product_id &&   item.itemList.map(childs=> <ExpansionPanel onChange={()=>this.openPanel(childs)} expanded={childs.id == this.state.panel} >
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                            >
                            <TwoItems
                                key={2}
                                label="Comment:"
                                content={childs.comment}
                                />
                            <TwoItems
                                key={2}
                                label="Cost:"
                                content={childs.purchaseCost}
                                />
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Typography>
                            <TextField id="standard-basic" label="Purchase Cost"
                            name="purchaseValue" value={purchaseValue} onBlur={()=>this.getValueChanged(index,item,childs.id,'p')}  onChange={this.handleChange}
                            />
                            <TextField id="standard-basic" label="Comment"
                            name="commentValue" value={commentValue} onBlur={()=>this.getValueChanged(index,item,childs.id,'c')}  onChange={this.handleChange}
                            />
                            </Typography>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>)})}
                </PaperWithScroll>
            </Grid>
        </Grid>
        <p style={{marginTop:'10px'}}></p>
        <Divider />
        <p style={{marginTop:'10px'}}></p>
        <Button onClick={this.submit} disabled={Iqty>0 ? false:true} variant="contained" color="primary">
            Confirm
        </Button>
            <Button onClick={this.submit} style={{marginLeft:'5px'}} variant="contained" color="">
            cancel
        </Button>
    </ContainerPapaer>}
</div>)
    }
}

const mapDispatchToProps = dispatch => ({
    getPurchasedProducts: () => dispatch(getPurchasedProducts()),
    getProducts: ()=> dispatch(getProducts())
   })
const mapStateToProps = state => ({
    products: state.simpleReducer.products,
    pending: state.simpleReducer.pending,
    productsv1: state.simpleReducer.productsv1,
    pendingv1: state.simpleReducer.pendingv1,
   })

export default connect(mapStateToProps, mapDispatchToProps)(Purchase)