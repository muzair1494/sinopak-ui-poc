import React from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { VendorsList, Title } from './../list/propslist'
import { Button, TextField, MenuItem, IconButton } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import MaterialTableDemo from './propsFunction';
import ClearIcon from '@material-ui/icons/Clear';

// const useStyles = (theme) => ({
//     paper: {
//         position: 'absolute',
//         width: 400,
//         backgroundColor: '#fff',
//         border: '2px solid #000',
//         boxShadow: theme.shadows[5],
//         padding: theme.spacing(2, 4, 3),
//     },
//     root: {
//         '& > *': {
//             margin: theme.spacing(1),
//             display: 'flex',
//             alignItems: 'column'
//         },
//     },
//     formControl: {
//         margin: theme.spacing(1),
//         minWidth: 120,
//     },
// });

class VendorList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vendors: [],
            loading: true,
            showModal: false,
            uuid: '',
            name: '',
            email: '',
            contact: '',
            address: '',
            nameError: false,
            emailErrorError: false,
            contactError: false,
            addressError: false,
        };
    }
    componentDidMount() {
        axios.get('https://demo4681389.mockable.io/vendors').then((res) => {
            this.setState({ vendors: res.data.vendors, loading: false })
            console.log(this.state.vendors)
        });
        const user = JSON.parse(localStorage.getItem('user'));
        console.log('user ==> ', user)
        console.log(user[0].email)
    }
    buttonCalled = (x) => {
        console.log(x, 'chek button')
    }
    handleModal = () => {
        this.setState({ showModal: !this.state.showModal })
        this.resetForm();
    };
    resetForm() {
        this.setState({
            name: '',
            email: '',
            contact: '',
            contactName: '',
            address: '',
            nameError: false,
            emailError: false,
            contactError: false,
            contactNameError: false,
            addressError: false,
            editModal: false,
            title: null,
            ntn: ''
        })
    }
    onSubmit = () => {
        let obj = {
            uuid: (this.state.vendors.length + 2).toString(),
            name: this.state.name,
            email: this.state.email,
            contact: this.state.contact,
            address: this.state.address
        };
        if (this.state.name && this.state.contact) {
            this.setState({ vendors: this.state.vendors.concat(obj) })
            this.resetForm();
            this.handleModal();
        }
        else if (this.state.name === '' || this.state.name === null) {
            this.setState({ nameError: !this.state.nameError })
        } else if (this.state.contact === '' || this.state.contact === null) {
            this.setState({ contactError: !this.state.contactError })
        }
    };
    onDelete = (e) => {
        console.log("Index == ", e);
        var array = [...this.state.vendors]; // make a separate copy of the array
        if (e !== -1) {
            array.splice(e, 1);
            this.setState({ vendors: array });
        }
    };
    handleEditModal = () => {
        this.setState({ editModal: !this.state.editModal })
    };
    index;
    onEdit = (e) => {
        this.handleEditModal();
        this.index = e;
        var array = [...this.state.vendors];
        this.setState({
            name: array[e].name,
            email: array[e].email,
            contact: array[e].contact,
            address: array[e].address,
        })
        console.log(this.index)
    };
    onEditSubmit = () => {
        var array = [...this.state.vendors];
        array[this.index].name = this.state.name;
        array[this.index].email = this.state.email;
        array[this.index].contact = this.state.contact;
        array[this.index].address = this.state.address;
        this.setState({ vendors: array, editModal: !this.state.editModal })
    };
    onTitleChange = (e) => {
        this.setState({ title: e })
        console.log(e);
    };
    handleRowAdd = (item) => {
        this.setState({ vendors: this.state.vendors.concat(item) })
    };
    handleRowEdit = (item) => {
        const index = this.state.vendors.map(e => e.uuid).indexOf(item.uuid);
        console.log("index : ", index)
        var array = [...this.state.vendors];
        array[index].name = item.name;
        array[index].email = item.email;
        array[index].contact = item.contact;
        array[index].address = item.address;
        this.setState({ vendors: array })
    };
    handleRowDelete = (item) => {
        const index = this.state.vendors.map(e => e.uuid).indexOf(item.uuid);
        var array = [...this.state.vendors];
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ vendors: array });
        }
    };
    render() {
        const { vendors } = this.state
        return (
            <div className="container-fluid" style={{ paddingTop: 30 }}>
                {/* <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                    <Title listTitle="Vendors List" />
                    <Button variant="contained" color="primary" onClick={this.handleModal}>
                        Add Vendor
                    </Button>
                </div> */}
                <MaterialTableDemo
                    data={vendors}
                    onRowAdd={this.handleRowAdd}
                    onAdd={this.handleModal}
                    onRowUpdate={this.handleRowEdit}
                    onRowDelete={this.handleRowDelete} />
                {/*                 
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    open={showModal}
                    onClose={this.handleModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={showModal}>
                        <div style={{
                            backgroundColor: "white",
                            width: '500px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                                <h3 className="modal-header-text">Create Vendor
                                <IconButton size="small" className="modal-header-icon" onClick={this.handleModal}>
                                        <ClearIcon />
                                    </IconButton></h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                <TextField
                                    required
                                    id="outlined-select-title"
                                    select
                                    label="Title"
                                    value={title}
                                    onChange={(val) => this.onTitleChange(val.target.value)}
                                    variant="outlined"
                                    fullWidth
                                    size="small">
                                    <MenuItem key="1" value="M/S">M/S</MenuItem>
                                    <MenuItem key="2" value="Mr.">Mr.</MenuItem>
                                    <MenuItem key="3" value="Miss">Miss</MenuItem>
                                    <MenuItem key="4" value="Mrs.">Mrs.</MenuItem>
                                </TextField>
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField required size="small" variant="outlined" id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Vendor Name" fullWidth value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField size="small" variant="outlined" id="filled-email" error={this.state.emailError} helperText={this.state.emailError ? 'required' : null} label="Email" fullWidth value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField required size="small" variant="outlined" id="filled-contact" error={this.state.contactError} helperText={this.state.contactError ? 'required' : null} label="Contact" fullWidth value={this.state.contact} onChange={e => this.setState({ contact: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField required size="small" variant="outlined" id="filled-contact-name" error={this.state.contactNameError} helperText={this.state.contactNameError ? 'required' : null} label="Contact Person Name" fullWidth value={this.state.contactName} onChange={e => this.setState({ contactName: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField required size="small" variant="outlined" id="filled-ntn" label="NTN" fullWidth value={this.state.ntn} onChange={e => this.setState({ ntn: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField size="small" variant="outlined" id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Address" fullWidth multiline rows="4" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>

                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.onSubmit} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.handleModal}>cancel</Button>
                                </div>
                            </div>
                        </div>
                    </Fade>
                </Modal>
                <Modal
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    open={editModal}
                    onClose={this.handleEditModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={editModal}>
                        <div style={{
                            backgroundColor: "white",
                            width: '400px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                            <h3 className="modal-header-text">Edit Vendor
                                <IconButton size="small" className="modal-header-icon" onClick={this.handleModal}>
                                        <ClearIcon />
                                    </IconButton></h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                <TextField
                                    id="outlined-select-title-edit"
                                    select
                                    label="Title"
                                    value={title}
                                    onChange={this.onTitleChange}
                                    variant="outlined"
                                    fullWidth>
                                    <MenuItem key="1" value="M/S">M/S</MenuItem>
                                    <MenuItem key="2" value="Mr.">Mr.</MenuItem>
                                    <MenuItem key="3" value="Miss">Miss</MenuItem>
                                    <MenuItem key="4" value="Mrs.">Mrs.</MenuItem>
                                </TextField>
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Vendor Name" fullWidth value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-email" label="Email" fullWidth value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-contact" error={this.state.contactError} helperText={this.state.contactError ? 'required' : null} label="Contact" fullWidth value={this.state.contact} onChange={e => this.setState({ contact: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField size="small" variant="outlined" id="filled-contact-name-edit" error={this.state.contactNameError} helperText={this.state.contactNameError ? 'required' : null} label="Contact Person Name" fullWidth value={this.state.contactName} onChange={e => this.setState({ contactName: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField size="small" variant="outlined" id="filled-ntn-edit" label="NTN" fullWidth value={this.state.ntn} onChange={e => this.setState({ ntn: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" label="Address" fullWidth multiline rows="4" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>

                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.onEditSubmit} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.handleEditModal}>cancel</Button>
                                </div>
                            </div>
                        </div>
                    </Fade>
                </Modal> */}
            </div >
        );
    }
}

export default VendorList;