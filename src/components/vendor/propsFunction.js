import React from 'react';
import MaterialTable from 'material-table';

export default function MaterialTableDemo(props) {
    const [state, setState] = React.useState({
        columns: [
            { title: 'Name', field: 'name' },
            { title: 'Email', field: 'email' },
            { title: 'Contact', field: 'contact' },
            { title: 'Contact Person', field: 'contact_person' },
            { title: 'NTN', field: 'ntn_number' },
            { title: 'Address', field: 'address' }
        ]
    });

    return (
        <MaterialTable
            title="Vendors List"
            columns={state.columns}
            data={props.data}
            editable={{
                onRowAdd: (newData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            props.onRowAdd(newData)
                            resolve();
                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            console.log("OLd ==> ", oldData);
                            console.log("New data ==> ", newData)
                            props.onRowUpdate(newData);
                            resolve();
                        }, 600);
                    }),
                onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                        setTimeout(() => {
                            props.onRowDelete(oldData);
                            resolve();
                        }, 600);
                    }),
            }}
        />
    );
}