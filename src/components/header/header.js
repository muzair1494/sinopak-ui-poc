import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import './header.css';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    anchor: {
        color: '#fff',
        textDecoration: 'none'
    }
}));


class Header extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            type:''
        };
    }
    componentDidMount() {
        const user = JSON.parse(localStorage.getItem('user'));
        this.setState({ username: user[0].username,  type: user[0].type })
    }
    handleLogout(){
        localStorage.removeItem('user');
        window.location.href = "/login";
    }
    render() {
        const {type} = this.state
        console.log(type)
        return (
            <div className={useStyles.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" className="title">
                            SINOPAK
                        </Typography>
                        <Typography>
                        <Button color="inherit" className={useStyles.title}>
                            <Link to="/vendors" color="inherit" className="anchor">Vendors</Link>
                        </Button>
                        {type === "admin" && <Button color="inherit" className={useStyles.title}>
                            <Link to="/users" color="inherit" className="anchor">Users</Link>
                        </Button>}
                        {/* <Button color="inherit">
                            <Link to="/purchaseOrder" color="inherit" className="anchor">Purchase</Link>
                        </Button>
                        <Button color="inherit">
                        <Link to="/GRN" color="inherit" className="anchor">GRN</Link>
                        </Button> */}
                        <Button color="inherit">
                            <Link to="/inventoryList" color="inherit" className="anchor">inventoryList</Link>
                        </Button>
                        <Button color="inherit">
                            <Link to="/AddTabs" color="inherit" className="anchor">WareHouse</Link>
                        </Button>
                        <Button color="inherit">
                            <Link to="/products" color="inherit" className="anchor">Products</Link>
                        </Button>
                        <Button color="inherit">
                            <Link to="/items" color="inherit" className="anchor">Items</Link>
                        </Button>
                        {type === "admin" &&  <Button color="inherit">
                            <Link to="/organization" color="inherit" className="anchor">Organization</Link>
                        </Button>}
                        </Typography>
                         {/* <Button color="inherit">
                            <Link to="/GRN" color="inherit" className="anchor">GRN</Link>
                        </Button>  */}
                        
                        <div className="user-details">
                        <p className="user-name">{this.state.username},</p>
                            <Button color="inherit">
                                <Link color="inherit" className="anchor" onClick={() => this.handleLogout()}>Logout</Link>
                            </Button>
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}
const anchor = {
    color: '#fff'
}
export default Header;