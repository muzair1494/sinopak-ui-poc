import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';


export const Title = ({ listTitle }) => <div style={{ margin: 0, textTransform: 'uppercase', fontSize: '21px' }}>{listTitle}</div>

export const VendorsList = (props) => {

    // props.onDelete = (e) => {
    //     console.log("Index == ", e); 
    // };
    return <TableRow key={props.item.uuid}>

        <TableCell>{props.item.name}</TableCell>
        <TableCell>{props.item.email}</TableCell>
        <TableCell>{props.item.contact}</TableCell>
        <TableCell>{props.item.address}</TableCell>
        <TableCell align="center">
            <IconButton aria-label="edit" size="small" onClick={() => props.onEdit(props.index)}>
                <EditIcon />
            </IconButton>
            <IconButton aria-label="delete" color="secondary" size="small" onClick={() => props.onDelete(props.index)}>
                <DeleteIcon />
            </IconButton>
        </TableCell>
        {/* <TableCell><button onClick={props.getcalled}>edit</button></TableCell> */}
    </TableRow>
}
