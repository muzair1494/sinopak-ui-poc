import React from 'react';
import axios from 'axios';
import 'react-confirm-alert/src/react-confirm-alert.css';
import MaterialTableUsers from './propsFunction';

class Users extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vendors: [],
            loading: true,
            showModal: false,
            uuid: '',
            name: '',
            email: '',
            contact: '',
            address: '',
            nameError: false,
            emailErrorError: false,
            contactError: false,
            addressError: false,
        };
    }
    componentDidMount() {
        axios.get('https://demo4681389.mockable.io/users').then((res) => {
            this.setState({ vendors: res.data.users, loading: false })
            console.log(this.state.vendors)
        });
        const user = JSON.parse(localStorage.getItem('user'));
        console.log('user ==> ', user)
        console.log(user[0].email)
    }
    handleRowAdd = (item) => {
        this.setState({ vendors: this.state.vendors.concat(item) })
    };
    handleRowEdit = (item) => {
        const index = this.state.vendors.map(e => e.uuid).indexOf(item.uuid);
        console.log("index : ", index)
        var array = [...this.state.vendors];
        array[index].name = item.name;
        array[index].email = item.email;
        array[index].contact = item.contact;
        array[index].address = item.address;
        this.setState({ vendors: array })
    };
    handleRowDelete = (item) => {
        const index = this.state.vendors.map(e => e.uuid).indexOf(item.uuid);
        var array = [...this.state.vendors];
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({ vendors: array });
        }
    };
    render() {
        const { vendors } = this.state
        return (
            <div className="container-fluid" style={{ paddingTop: 30 }}>
                <MaterialTableUsers
                    data={vendors}
                    onRowAdd={this.handleRowAdd}
                    onAdd={this.handleModal}
                    onRowUpdate={this.handleRowEdit}
                    onRowDelete={this.handleRowDelete} />
            </div >
        );
    }
}

export default Users;