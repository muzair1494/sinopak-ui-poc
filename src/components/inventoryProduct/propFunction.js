import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  rootPag: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export const SimpleExpansionPanel = ({inventory,viewDetail,showListId,showListProductId,showListItem,randId}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {inventory.map((item)=>{
            return <ExpansionPanel key={item.name}>
                {/* parent row */}
                <ExpansionPanelSummary
                  style={{background:'#F2EEED',color:'black',margin:'2px'}}
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel2a-content"
                  id="panel2a-header"
                >
                  {/* child row */}
                  {getProductRow(item,classes)}
                </ExpansionPanelSummary>
                <ExpansionPanelDetails style={{display:'block'}}>
                  <Typography>
                  {/* grand child row batch items */}
                  {getBatchItems(showListId,showListProductId,{showListItem},item,{viewDetail},randId)}
                  </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>})}
    </div>
  );
}

const getProductRow = (item,classes)=>{
    return <Grid container spacing={2}>
      <Grid item xs={2}><Typography className={classes.heading}>{item.image}</Typography></Grid>
      <Grid item xs={2}><Typography className={classes.heading}>{item.name}</Typography></Grid>
      <Grid item xs={2}><Typography className={classes.heading}>{item.color}</Typography></Grid>
      <Grid item xs={2}><Typography className={classes.heading}>{item.shape}</Typography></Grid>
      <Grid item xs={2}><Typography className={classes.heading}>
      <Button variant="contained" color="secondary">Batches</Button>
      </Typography></Grid>
  </Grid>
}

const getBatchItems =(showListId,showListProductId,{showListItem},item,{viewDetail},randId)=>{
    return <div>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <TableContainer component={Paper}>
            <Table  size="medium" aria-label="a dense table">
              <TableBody>
              {item.range != undefined && item.range.map(items => (
                <TableRow style={{background:randId==items.randId? '#52ADA4':randId != null ? '#D7DEDD':''}}  key={items.range}>
                  <TableCell align="right"><b>Batch - {items.id}</b> </TableCell>
                    <TableCell component="th" scope="row">
                      <b>Cost Range:</b>
                    </TableCell>
                    <TableCell align="right">{items.range}</TableCell>
                    <TableCell align="right"><b>Quantity:</b></TableCell>
                    <TableCell align="right">{items.val}</TableCell>
                    <TableCell align="right"><Button variant="contained" onClick={()=>showListItem(item['product-uuid'],items.range,items.randId)} color="primary">Items</Button></TableCell>
                    <TableCell>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
        </TableContainer>
      </Grid>
      <Grid item xs={6}>
        {item.range != undefined && item.range.map(items => (
          showListId == items.range && showListProductId ==  item['product-uuid'] ? showListItems(item,items.range,{viewDetail}): null
        ))}
      </Grid>
    </Grid>
  </div>
}

const showListItems = (item,range,{viewDetail})=>{
  return <div>
      <TableContainer style={{background:'#79C2B7'}}  component={Paper}>
        <Table  size="large" aria-label="a dense table">
          <TableBody>
            {item.list != undefined && item.list.map(items => (
            items.id == range ?  <TableRow key={items.barcode}>
                  <TableCell component="th" scope="row">
                    <b>Barcode:</b>
                  </TableCell>
                  <TableCell align="right">{items.barcode}</TableCell>
                  <TableCell align="right"><b>status:</b></TableCell>
                  <TableCell align="right">{items.status}</TableCell>
                  <TableCell align="right"><b>CummulativeCost:</b></TableCell>
                  <TableCell align="right">{items.cost}</TableCell>
                  <TableCell align="right"><Button variant="contained" onClick={()=>viewDetail(items.id == range ? item['product-uuid']:null,items,item)} color="primary">Detail</Button></TableCell>
                </TableRow>: null
              ))}
          </TableBody>
        </Table>
      </TableContainer>
  </div>
}


export const getInventoryRange=(inputValue,inventory)=>{
  inventory.map((item)=>{
    item.range = []
    item.list=[]
    return item
  })
  let {count,y,first,second} = 0
  let x = Number(inputValue)
  let boolVal =false
  let idCount =0;
  for(var i=0;i<inventory.length;i++){
      idCount =0
      for(var j=0;j<inventory[i].items.length; j++){
          //get cost from every inventory
          let cost = inventory[i].items[j].cumulativeCost
          x = inputValue-1
          count = y = 0
          boolVal =false
          while(boolVal != true){
              first = y
              second= x 
              if(cost <= second){
                  boolVal= true
                  inventory[i].range.map((item)=>{
                      if(item.range == `${first}-${second}`){
                          count++
                          let items ={
                            id: `${first}-${second}`,
                            barcode: inventory[i].items[j].barcode, 
                            cost: inventory[i].items[j].cumulativeCost,
                            status: inventory[i].items[j].status,
                            shipment: inventory[i].items[j].cost.shippment_cost,
                            delivery: inventory[i].items[j].cost.delivery_cost,
                            tax: inventory[i].items[j].cost.tax_rate,
                            vendor: inventory[i].items[j].vendor.name
                          }
                          inventory[i].list.push(items)
                          return item.val = item.val+1
                      }})
                  if(count == 0){
                      let items ={
                        id: `${first}-${second}`,
                        barcode: inventory[i].items[j].barcode,
                        cost: inventory[i].items[j].cumulativeCost,
                        status: inventory[i].items[j].status,
                        shipment: inventory[i].items[j].cost.shippment_cost,
                        delivery: inventory[i].items[j].cost.delivery_cost,
                        tax: inventory[i].items[j].cost.tax_rate,
                        vendor: inventory[i].items[j].vendor.name
                      }
                      inventory[i].list.push(items)
                      idCount = idCount+1
                      let value = {range:`${first}-${second}`,val:1,id: idCount,randId:`${inventory[i]['product-uuid']}${idCount}`}
                      inventory[i].range.push(value) 
                  }
              }
              y= x+1 
              x =x+inputValue
          }
      }
      let str = inventory[i].range[0].range.trim()
      str = str.substring(0,str.indexOf("-"));
      console.log(Number(str)+1,'inventory[i].range')
      // inventory[i].range.sort((a, b) => `${a.range[0]+a.range[1]}` - `${b.range[0]+b.range[1]}`); 
  }
}


