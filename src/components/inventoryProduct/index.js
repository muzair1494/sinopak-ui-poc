import React, { Component } from 'react'
import { SimpleExpansionPanel, getInventoryRange } from './propFunction'
import Grid from '@material-ui/core/Grid';
import { GetSearchData } from './../../common/searchINput'
import TableModal from './../../common/modalClassBased'
import TransitionsModal from './../../common/modal'
import axios from 'axios';
import { connect } from 'react-redux';
import { simpleAction } from './../../actions/simpleAction';
import { Pagination } from '@material-ui/lab';
import { InputField } from './../../common/inputField'
import Button from '@material-ui/core/Button'
import { Title } from '../list/propslist';

class InventoryList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inventory: [],
            temp: [],
            search: '',
            batchSearch: null,
            showListId: null,
            showListProductId: null,
            open: false,
            detail: {},
            randId: null,
            contact: '',
            address: '',
            email: '',
            contact: '',
            openForm: false
        }
    }
    componentDidMount() {
        this.simpleAction()
        // fetch inventory data from server
        axios.get('http://demo1951402.mockable.io/inventory').then((res) => {
            this.setState({ inventory: res.data.inventory }, () => {
                //add empty array as an range
                const { inventory } = this.state
                //byDefault it gets the value on range wise: 50
                getInventoryRange(50, inventory)
                this.setState({ inventory })
            })
        })
    }
    componentWillReceiveProps(nextprops) {
        console.log(nextprops.simpleResult, 'nextprops')
    }
    simpleAction = () => {
        this.props.simpleAction();
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, open: false })
    }
    getSearched = (e) => {
        this.setState({ [e.target.name]: e.target.value, open: false })
    }
    getBatches = (value, val) => {
        this.setState({ open: false, batchSearch: value.target.value }, () => {
            const { batchSearch, inventory } = this.state
            if (!isNaN(batchSearch) && batchSearch >= 10) {
                getInventoryRange(Number(batchSearch.trim()), inventory)
                this.setState({ inventory, open: false })
            } else {
                alert('input value should be number which is greater than 9')
            }
        })
    }
    getDetailId = (prodId, items, item) => {
        let values = {
            product_name: item.name,
            shape: item.shape,
            color: item.color,
            image: item.image,
            barcode: items.barcode,
            range: items.id,
            cost: items.cost,
            status: items.status,
            shipment: items.shipment,
            delivery: items.delivery,
            tax: items.tax,
            vendor: items.vendor
        }
        console.log(values, 'prodIdprodId2')
        this.setState({ detail: values, open: true })
    }
    getListIds = (prodId, rangeId, randId) => {
        this.setState({ randId, showListId: rangeId, showListProductId: prodId, open: false })
    }
    getLables = (a, b, c) => {
        console.log(a, b, c)
    }
    submit = () => {
        this.setState({ openForm: false })
        const { name, address, contact, email } = this.state
        const values = { name, address, contact, email }
        console.log(values)
    }
    cancelSubmit = () => { this.setState({ openForm: false }) }
    openNewForm = () => {
        this.setState({ openForm: true })
    }
    onBlur = (key, val) => { console.log(key) }
    render() {
        const { openForm, randId, detail, open, inventory, search, batchSearch, showListId, showListProductId } = this.state
        let filteredData = inventory.filter(items => {
            return (items.name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1);
        })
        return (
            <div className="container-fluid">
                <Grid container spacing={0}>
                    <Grid item xs={2}>
                        <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                            <Title listTitle="Inventory List" />
                        </div>
                    </Grid>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={2}>
                        <GetSearchData
                            val="InventoryProduct"
                            onBlur={this.onBlur}
                            name="search"
                            search={search}
                            getSearched={this.getSearched}
                            title="Search By Product Name" />
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={3}>
                        <GetSearchData
                            val="InventoryProductCost"
                            name="batchSearch"
                            search={batchSearch}
                            onBlur={this.getBatches}
                            getSearched={this.getSearched} title="Group By Cost" />
                    </Grid>
                </Grid>
                <SimpleExpansionPanel
                    showListProductId={showListProductId}
                    showListId={showListId}
                    showListItem={this.getListIds}
                    inventory={filteredData} viewDetail={this.getDetailId}
                    randId={randId}
                />
                <TableModal
                    open={open}
                    detail={detail}
                />
                <Pagination page={4} style={{ float: 'right', marginTop: 15 }} getItemAriaLabel={this.getLables} count={10} size="large" color="secondary" />
                <TransitionsModal openForm={openForm} title="Add New Item" >
                    <InputField change={this.handleChange} name="name" lable="username" />
                    <InputField change={this.handleChange} name="address" lable="address" />
                    <InputField change={this.handleChange} name="email" lable="email" />
                    <InputField change={this.handleChange} name="contact" lable="contact no." />
                    <Button onClick={this.cancelSubmit} style={{ float: 'right', marginLeft: '5px' }} variant="contained" color="">
                        cancel
                </Button>
                    <Button onClick={this.submit} style={{ float: 'right' }} variant="contained" color="primary">
                        Submit
                </Button>
                </TransitionsModal>

            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    simpleAction: () => dispatch(simpleAction())
})
const mapStateToProps = state => ({
    simpleResult: state.simpleReducer.result
})

export default connect(mapStateToProps, mapDispatchToProps)(InventoryList)