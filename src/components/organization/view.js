import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root}>
        <h2>View Organization Details</h2>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Company Name
        </Typography>
        <Typography variant="h5" component="h2">
          SINOPAK
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Company Branch
        </Typography>
        <Typography variant="h5" component="h2">
          Dubai
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Company NTN
        </Typography>
        <Typography variant="h5" component="h2">
          1234567
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Company Adress
        </Typography>
        <Typography variant="h5" component="h2">
        D-54 Additional Ambernath 421506 Sparklet India
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          Company Contact No.
        </Typography>
        <Typography variant="h5" component="h2">
        +92 123-123-321
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}
