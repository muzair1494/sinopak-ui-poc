import React from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { VendorsList, Title } from './../list/propslist'
import { Button, TextField } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import SimpleCard from './view'

// const useStyles = (theme) => ({
//     paper: {
//         position: 'absolute',
//         width: 400,
//         backgroundColor: '#fff',
//         border: '2px solid #000',
//         boxShadow: theme.shadows[5],
//         padding: theme.spacing(2, 4, 3),
//     },
//     root: {
//         '& > *': {
//             margin: theme.spacing(1),
//             display: 'flex',
//             alignItems: 'column'
//         },
//     },
//     formControl: {
//         margin: theme.spacing(1),
//         minWidth: 120,
//     },
// });

class VendorList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vendors: [],
            loading: true,
            showModal: false,
            uuid: '',
            name: '',
            email: '',
            contact: '',
            address: '',
            nameError: false,
            emailErrorError: false,
            contactError: false,
            addressError: false,
        };
    }
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/vendors').then((res) => {
            this.setState({ vendors: res.data.vendors, loading: false })
            console.log(this.state.vendors)
        });
        const user = JSON.parse(localStorage.getItem('user'));
        console.log('user ==> ', user)
        console.log(user[0].email)
    }
    buttonCalled = (x) => {
        console.log(x, 'chek button')
    }
    handleModal = () => {
        this.setState({ showModal: !this.state.showModal })
        this.resetForm();
    };
    resetForm() {
        this.setState({
            name: '',
            email: '',
            contact: '',
            address: '',
            nameError: false,
            emailError: false,
            contactError: false,
            addressError: false,
            editModal: false
        })
    }
    onSubmit = () => {
        let obj = {
            uuid: (this.state.vendors.length + 2).toString(),
            name: this.state.name,
            email: this.state.email,
            contact: this.state.contact,
            address: this.state.address
        };
        if (this.state.name && this.state.email && this.state.contact && this.state.address) {
            this.setState({ vendors: this.state.vendors.concat(obj) })
            this.resetForm();
            this.handleModal();
        }
        else if (this.state.name === '' || this.state.name === null) {
            this.setState({ nameError: !this.state.nameError })
        } else if (this.state.email === '' || this.state.email === null) {
            this.setState({ emailError: !this.state.emailError })
        } else if (this.state.contact === '' || this.state.contact === null) {
            this.setState({ contactError: !this.state.contactError })
        } else if (this.state.address === '' || this.state.address === null) {
            this.setState({ addressError: !this.state.addressError })
        }
    };
    onDelete = (e) => {
        console.log("Index == ", e);
        var array = [...this.state.vendors]; // make a separate copy of the array
        if (e !== -1) {
            array.splice(e, 1);
            this.setState({ vendors: array });
        }
    };
    handleEditModal = () => {
        this.setState({ editModal: !this.state.editModal })
    };
    index;
    onEdit = (e) => {
        this.handleEditModal();
        this.index = e;
        var array = [...this.state.vendors];
        this.setState({
            name: array[e].name,
            email: array[e].email,
            contact: array[e].contact,
            address: array[e].address,
        })
        console.log(this.index)
    };
    onEditSubmit = () => {
        var array = [...this.state.vendors];
        array[this.index].name = this.state.name;
        array[this.index].email = this.state.email;
        array[this.index].contact = this.state.contact;
        array[this.index].address = this.state.address;
        this.setState({ vendors: array, editModal: !this.state.editModal })
    };
    render() {
        const { showModal, editModal } = this.state
        return (
            <div className="container-fluid">
                <SimpleCard />
                <div style={{ flexDirection: 'row', justifyContent: 'space-between', display: 'flex', marginTop: '30px', marginBottom: '20px' }}>
                    <Title listTitle="Organization List" />
                    {/* <Button variant="contained" color="primary" onClick={this.handleModal}>
                        Add company
                    </Button> */}
                </div>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                
                                <TableCell>Name</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Contact</TableCell>
                                <TableCell>Address</TableCell>
                                <TableCell align="center">Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.vendors.map((item, index) => (
                                <VendorsList onEdit={this.onEdit} index={index} onDelete={this.onDelete} key={item.uuid} getcalled={() => this.buttonCalled(item.uuid)} item={item} />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                {this.state.loading && <div className="loadingTr">Loading ...</div>}
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    open={showModal}
                    onClose={this.handleModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={showModal}>
                        <div style={{
                            backgroundColor: "white",
                            width: '400px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                                <h3 style={{ margin: 0 }}>Create</h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                <TextField id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Name" fullWidth value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-email" error={this.state.emailError} helperText={this.state.emailError ? 'required' : null} label="Email" fullWidth value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-contact" error={this.state.contactError} helperText={this.state.contactError ? 'required' : null} label="Contact" fullWidth value={this.state.contact} onChange={e => this.setState({ contact: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Address" fullWidth multiline rows="4" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>

                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.onSubmit} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.handleModal}>cancel</Button>
                                </div>
                            </div>
                        </div>
                    </Fade>
                </Modal>
                <Modal
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    open={editModal}
                    onClose={this.handleEditModal}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={editModal}>
                        <div style={{
                            backgroundColor: "white",
                            width: '400px',
                            border: '2px solid #fff'
                        }}>
                            <div style={{ padding: '15px', borderBottom: '1px solid #ccc' }}>
                                <h3 style={{ margin: 0 }}>Edit</h3>
                            </div>
                            <div style={{ padding: '15px', }}>
                                <TextField id="standard-name" error={this.state.nameError} helperText={this.state.nameError ? 'required' : null} label="Name" fullWidth value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-email" error={this.state.emailError} helperText={this.state.emailError ? 'required' : null} label="Email" fullWidth value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-contact" error={this.state.contactError} helperText={this.state.contactError ? 'required' : null} label="Contact" fullWidth value={this.state.contact} onChange={e => this.setState({ contact: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>
                                <TextField id="filled-address" error={this.state.addressError} helperText={this.state.addressError ? 'required' : null} label="Address" fullWidth multiline rows="4" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} />
                                <div style={{ marginTop: '10px' }}></div>

                                <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between' }}>
                                    <Button variant="contained" color="primary" onClick={this.onEditSubmit} style={{ width: '49%' }}>
                                        Submit
                                    </Button>
                                    <Button variant="contained" style={{ width: '49%' }} onClick={this.handleEditModal}>cancel</Button>
                                </div>
                            </div>
                        </div>
                    </Fade>
                </Modal>
            </div >
        );
    }
}

export default VendorList;