import React, { Component } from 'react';
import './login.css';
import { TextField, Button } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [],
            username: '',
            password: '',
            loginType: '',
            errorMsg: false
        };
    }
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/loginv1').then((response) => {
            this.setState({ users: response.data.login });
        }).catch(err => console.log('Error in login api! ', err));
    }
    handleLoginType = (event) => {
        this.setState({ loginType: event.target.value })
    };
    handleLoginSubmit = () => {
        const { username, password, loginType } = this.state;
        const userData = this.state.users.filter(user => user.email === username && user.password === password);
        //console.log(userData)
        if (userData.length !== 0) {
            localStorage.setItem('user', JSON.stringify(userData));
            window.location.href = "/vendors";
            this.setState({ errorMsg: false })
        } else {
            this.setState({ errorMsg: true })
        }
    };
    render() {
        const { username, password, loginType, errorMsg } = this.state;
        return (
            <div className="loginContainer">
                <div className="loginBox">
                    <div className="logoName">
                        <h4>LOGIN TO SINOPAK</h4>
                    </div>
                    <div className="loginField">
                        <TextField
                            required
                            id="filled-username"
                            label="Email Address"
                            variant="outlined"
                            fullWidth
                            onChange={(val) => this.setState({ username: val.target.value.toLowerCase() })}
                        />
                    </div>
                    <div className="loginField">
                        <TextField
                            required
                            id="filled-password"
                            label="Password"
                            variant="outlined"
                            fullWidth
                            onChange={(val) => this.setState({ password: val.target.value })}
                        />
                    </div>
                    {/* <div className="loginField">
                        <TextField
                            id="outlined-select-currency"
                            select
                            label="Login Type"
                            value={loginType}
                            onChange={this.handleLoginType}
                            variant="outlined"
                            fullWidth
                        >
                            <MenuItem key="1" value="admin">
                                Admin
                            </MenuItem>
                            <MenuItem key="2" value="employee">
                                Employee
                            </MenuItem>
                        </TextField>
                    </div> */}
                    <div className="">
                        <Button
                            variant="contained"
                            color="primary"
                            fullWidth
                            disabled={username === '' || password === ''}
                            className="loginBtn"
                            onClick={this.handleLoginSubmit}>Login</Button>
                    </div>
                    {errorMsg && <Alert severity="error">Incorrect Credentials</Alert>}
                </div>
            </div>
        );
    }
}

export default Login