import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import {TwoItems} from './../../common/twoItemRow'
import FileUpload from './../../common/uploadFIle'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import DropDown from './../../common/dropDownSelect'
import MenuItem from '@material-ui/core/MenuItem';
import FontAwesome from 'react-fontawesome'

import Radio from '@material-ui/core/Radio';


// entry_person:'get from loggedin username', done
// entry_date:'system auto date generate', done
// Imported_from:'', done
// receiving_person:'', done
// receiving_date:'', done
// received: 'no. of items that received', done
// ordered: 'no. of items that ordered', //total length of items 
// warehouse_location:'', done
// status:{
//         pending,
//         stored
//        }
// document_upload:'', done
// tax:'', done
// shipment_cost:'', done
// delivery_cost:'', done
// other_cost:'', done
// remaining_payment:'' done
// total_cost:'' done

const useStyles = makeStyles((theme) => ({
  rootText: {
    '& .MuiInputBase-input MuiInput-input': {
      padding:0,
    },
  },
    root: {
      width: '100%',
      maxWidth: 1000,
      backgroundColor: theme.palette.background.paper,
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    modalpaper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));

const cost_type = ["shipment cost","delivery cost","tax","other cost"]

const getModalStyle =()=> {
  const top = 50 ;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

export const ExpansionPanelGRN = ({data,addCost,items,mySet,handleToggle,checked,chekcedId})=>{
  const classes = useStyles();
  
return data.length == 0 ? mySet.map((value)=>{
  // const id= value.uuid
  return  <ExpansionPanel key={value} style={{width:'100%'}}>
  {/* parent row */}
  <ExpansionPanelSummary
    style={{background:'#F2EEED',color:'black',margin:'2px'}}
    expandIcon={<ExpandMoreIcon />}
    aria-controls="panel2a-content"
    id="panel2a-header"
  >
    <Grid container spacing={2}>
      <Grid item xs={4}><Typography className={classes.heading}><b>Vendor:</b></Typography></Grid>
      <Grid item xs={4}><Typography className={classes.heading}>{value}</Typography></Grid>
      {/* <Grid item xs={2}><Typography className={classes.heading}><b>Qty:</b></Typography></Grid>
      <Grid item xs={2}><Typography className={classes.heading}>{value}</Typography></Grid> */}
      <Grid item xs={2}><Typography className={classes.heading}>
      <Button style={{width:'210px'}} variant="contained" color="secondary">Purchase Orders</Button>
      </Typography></Grid>
  </Grid>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails style={{display:'block'}}>
        <TableContainer  component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow style={{background:'#318C9B'}}>
            <TableCell align="left">Purchase Order</TableCell> 
            <TableCell align="left">Product</TableCell>
            <TableCell align="left">Barcode</TableCell>
            <TableCell align="left">Qty</TableCell>
            {/* <TableCell align="left">Shape</TableCell>
            <TableCell align="left">Color</TableCell> */}
            <TableCell align="left">Purchase Cost</TableCell>
            <TableCell align="left">Description</TableCell>
            <TableCell align="left">Customize Cost</TableCell>
            <TableCell align="left">Total Cost</TableCell>
            <TableCell align="left">Select Items</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {items.map((val,index) => {
          const labelId = `${val.uuid}`;
          return val.vendor_name === value ? 
          val.item.map((content)=>{
            return <TableRow 
            //  onClick={()=>getOneProduct(item,name)}
             style={{background:'rgb(215, 222, 221)',marginTop:'2px'}} button key={labelId}>
              <TableCell align="left">{val.purchase_order_no}</TableCell>
              <TableCell align="left" >{content.product_name}</TableCell>
              <TableCell align="left" >{content.product_barcode}</TableCell>
              <TableCell align="left" >{content.product_quantity}</TableCell>
              {/* <TableCell align="left" >{val.item.shape}</TableCell>
              <TableCell align="left" >{val.item.color}</TableCell> */}
              <TableCell align="left" >{content.product_purchase_cost}</TableCell>
              <TableCell align="left" >{content.product_description}</TableCell>
              <TableCell align="left" ><ButtonComponent name="Add Cost" click={()=>addCost(content.product_barcode,val.uuid)}  /></TableCell>
              <TableCell align="left" >{content.count}</TableCell>
              <TableCell align="left"><Checkbox
                  edge="end"
                  onChange={handleToggle(content.product_barcode,val.uuid)}
                  checked={checked.indexOf(content.product_barcode) !== -1 && chekcedId.indexOf(val.uuid) !== -1}
                  inputProps={{ 'aria-labelledby': labelId }}
        /></TableCell>
        </TableRow>
          }) 
           : null
        })}
          </TableBody>
         </Table>
        </TableContainer>
        
                </ExpansionPanelDetails>
               
    </ExpansionPanel>}) : 
     mySet.map((value)=>{
      // const id= value.uuid
      return data[0].vendor_name == value && <ExpansionPanel key={value} style={{width:'100%'}}>
      {/* parent row */}
      <ExpansionPanelSummary
        style={{background:'#F2EEED',color:'black',margin:'2px'}}
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Grid container spacing={2}>
          <Grid item xs={2}><Typography className={classes.heading}><b>Vendor:</b></Typography></Grid>
          <Grid item xs={2}><Typography className={classes.heading}>{value}</Typography></Grid>
          <Grid item xs={2}><Typography className={classes.heading}><b>Qty:</b></Typography></Grid>
          <Grid item xs={2}><Typography className={classes.heading}>{value}</Typography></Grid>
          <Grid item xs={2}><Typography className={classes.heading}>
          <Button style={{width:'210px'}} variant="contained" color="secondary">Purchase Orders</Button>
          </Typography></Grid>
      </Grid>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{display:'block'}}>
            <TableContainer  component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow style={{background:'#318C9B'}}>
                <TableCell align="left">Purchase Order</TableCell> 
                <TableCell align="left">Product</TableCell>
                <TableCell align="left">Barcode</TableCell>
                <TableCell align="left">Qty</TableCell>
                <TableCell align="left">Purchase Cost</TableCell>
                <TableCell align="left">Comment</TableCell>
                <TableCell align="left">Customize Cost</TableCell>
                <TableCell align="left">Total Cost</TableCell>
                <TableCell align="left">Select Items</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {items.map((val,index) => {
              const labelId = `${val.uuid}`;
              return val.vendor_name === value ? 
              val.item.map((content)=>{
                return<TableRow 
                //  onClick={()=>getOneProduct(item,name)}
                 style={{background:'rgb(215, 222, 221)',marginTop:'2px'}} button key={labelId}>
                  <TableCell align="left">{val.purchase_order_no}</TableCell>
                  <TableCell align="left" >{content.product_name}</TableCell>
                  <TableCell align="left" >{content.product_barcode}</TableCell>
                  <TableCell align="left" >{content.product_quantity}</TableCell>
                  <TableCell align="left" >{content.product_purchase_cost}</TableCell>
                  <TableCell align="left" >{content.product_description}</TableCell>
                  <TableCell align="left" ><ButtonComponent name="Add Cost" click={()=>addCost(content.product_barcode,val.uuid)}  /></TableCell>
                  <TableCell align="left" >{content.count}</TableCell>
                  <TableCell align="left"><Checkbox
                      edge="end"
                      onChange={handleToggle(content.product_barcode,val.uuid)}
                      checked={checked.indexOf(content.product_barcode) !== -1 && chekcedId.indexOf(val.uuid) !== -1}
                      inputProps={{ 'aria-labelledby': labelId }}
            /></TableCell>
            </TableRow>})
               : null
            })}
              </TableBody>
             </Table>
            </TableContainer>
            
                    </ExpansionPanelDetails>
                   
        </ExpansionPanel>})


}

export const ModalCost = ({blur,submit,otherCost,taxCost,deliveryCost,shipCost,enterCosts,addNewItem,open,costType,change,cancel})=>{
  const classes = useStyles();
  return <Modal
open={open}
// onClose={handleClose}
aria-labelledby="simple-modal-title"
aria-describedby="simple-modal-description"
>
<div style={getModalStyle()} className={classes.modalpaper}>
<Grid container spacing={2}>
    <Grid item xs={6}>
      <FontAwesome
      onClick={()=>addNewItem(costType)}
      className="super-crazy-colors"
      name="plus-circle"
      // cssModule={faStyles}
      size="3x"
      // spin
      style={{marginTop:'10px',color:"blue" ,textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
      />
    </Grid>
    <Grid item xs={6}>
      <DropDown 
      floating="right"
      title="Cost Type"
      value={costType}
      name="costType"
      change={change}
      >
        {cost_type.map(item=><MenuItem value={item}>{item}</MenuItem>)}
        
      </DropDown>
      </Grid>
      {enterCosts.length >0 && enterCosts.map(item=>{
      return item.key === "shipment cost" ?
      <ShipmentCost name="shipCost" shipCost={shipCost}  change={change}   />:
      item.key === "delivery cost" ?
      <DeliveryCost deliveryCost={deliveryCost}   change={change} />:
      item.key === "tax" ?
      <TaxCost  taxCost={taxCost} change={change}  />:
      item.key === "other cost"?
      <OtherCost otherCost={otherCost} change={change}   />: null
    })}
    <Grid item xs={12}>
      <Button 
      onClick={submit} 
      variant="contained" color="primary">
        Submit            
      </Button> 
      <Button
      style={{marginLeft:'5px'}}
       onClick={cancel} 
        variant="contained" color="">
          Cancel
      </Button>
      </Grid>
 
  </Grid>
</div>
</Modal>}


export const ItemList=({items,handleToggle,chekcedId,checked,selectId,change,editCost,editCost2,editCost3,editId,editId2,editId3,name,name2,setCost})=>{
    const classes = useStyles();
    return (
        <Paper elevation={3} style={{width:'80%',maxHeight:'300px',overflowY:'scroll'}} >
      <List dense className={classes.root}>
        {items.map((value,index) => {
          const labelId = `checkbox-list-secondary-label-${value.uuid}`;
          return (
            <ListItem key={value.uuid} button>
              {/* <ListItemAvatar>
                <Avatar
                  alt={`Avatar n°${index + 1}`}
                  src={`/static/images/avatar/${index + 1}.jpg`}
                />
              </ListItemAvatar> */}
              <ListItemText id={labelId} primary={value.item.product_name} />
              <ListItemText id={labelId} primary={value.item.product_barcode} />
              <ListItemText id={labelId} primary={value.item.vendor} />
              <ListItemText id={labelId} primary={ 
              <TextField id="standard-basic"
              onFocus={()=>selectId(value.uuid,value.item.product_purchase_cost,'ship')}
              onChange={change}
              onBlur={()=>setCost(editCost,'ship')}
              name={name}
              label="cost" value={value.uuid == editId ? editCost: value.item.product_purchase_cost} />} />
              <ListItemText id={labelId} primary={ 
              <TextField id="standard-basic"
              onFocus={()=>selectId(value.uuid,value.cost.othercost,'other')}
              onChange={change}
              onBlur={()=>setCost(editCost2,'other')}
              name={name2}
              label="other cost" value={value.uuid == editId2 ? editCost2: value.cost.othercost} />} />
              <ListItemText id={labelId} primary={ 
              <TextField id="standard-basic"
              onFocus={()=>selectId(value.uuid,value.item.product_description,'comment')}
              onChange={change}
              onBlur={()=>setCost(editCost3,'comment')}
              name="commentItem"
              label="comment" value={value.uuid == editId3 ? editCost3: value.item.product_description} />} />
              <ListItemSecondaryAction>
                <Checkbox
                  edge="end"
                  onChange={handleToggle(value.uuid,value.uuid)}
                  checked={checked.indexOf(value.uuid) !== -1 }
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
      </List>
      </Paper>
    );
}

export const ItemDetail =({addCost,imported_location,receiving_date,status_type,ordered_date,order_items,adv_pay,seal_no,receive_person,entry_date,receivedItems,handleCapture,sumOfCost,change,comment,grn_no,shipment,tax,delivery,totalCost,username,entryDate,sysId,grn_date})=>{
    return <div style={{width:'100%',maxHeight:'500px',overflowY:'scroll'}}> 
    <Paper  elevation={3} style={{width:'100%'}}>
        
            <TwoItems
             key={1}
             label="Seal No:"
             content={<TextField label="seal no" value={seal_no} style={{float:'right'}} id="standard-basic"
             onChange={change}
             name="seal_no"
              />}
            />
            <TwoItems
             key={2}
             label="Shippment Channel:"
             content={<TextField label="Ship Via" value={shipment} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="ship_channel"
                />}
            />
            <TwoItems
             key={3}
             label="Imported Location:"
             content={<TextField label="Imported From" value={imported_location} style={{float:'right',width:'70%'}} id="standard-basic"
               onChange={change}
               name="imported_location"
                />}
            /> 
        {/* <TwoItems
             key={2}
             label="GRN_NO:"
             content={<TextField label="grn_no" value={grn_no} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="grn_no"
                />}
            /> */}
         {/* <TwoItems
             key={3}
             label="GRN_Date:"
             content={<TextField type="date"  value={grn_date} style={{float:'right',width:'70%'}} id="standard-basic"
               onChange={change}
               name="grn_date"
                />}
            /> */}
            <TwoItems
             key={1}
             label="Receiving Person:"
             content={<TextField label="receive person" value={receive_person} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="receive_person"
                />}
            />
            <TwoItems
             key={3}
             label="Receiving_date:"
             content={<TextField type="date"  value={grn_date} style={{float:'right',width:'70%'}} id="standard-basic"
               onChange={change}
               name="receiving_date"
                />}
            />
            <TwoItems
             key={1}
             label="Comment:"
             content={<TextField label="comment" value={comment} style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="comment"
                />}
            />
        <TwoItems
             key={7}
             label="Ordered Item:"
             content={<p style={{float:'right'}}>{order_items}</p>}
            />
            <TwoItems
             key={7}
             label="Ordered date:"
             content={<p style={{float:'right'}}>{ordered_date}</p>}
            />
        <TwoItems
             key={4}
             label="Total Received Items:"
             content={<p style={{float:'right'}}>{receivedItems}</p>}
            />
        <TwoItems
             key={5}
             label="Warehouse Location:"
             content={<p style={{float:'right'}}>Main warehouse SUI</p>}
            />
             
        {/* <TwoItems
             key={6}
             label="Entry Date:"
             content={<p style={{float:'right'}}>{entryDate}</p>}
            />
        <TwoItems
             key={7}
             label="Entry User's name:"
             content={<TextField 
              value="Uzair"
              // value={username}
               label="username" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="username"
                />}
            /> */}
        <TwoItems
             key={1}
             label="Document:"
             content={<FileUpload 
                fl="right"
                change={handleCapture}
                />}
            />
        {/* <TwoItems
             key={1}
             label="Shipment Cost:"
             content={<TextField value={shipment} label="shipment cost" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="shipment"
               onBlur={sumOfCost}
                />}
            /> */}
            {/* <TwoItems
             key={1}
             label="Advance Payment:"
             content={<p style={{float:'right'}}>{adv_pay+'%'}</p>}
            /> */}
            {/* <TwoItems
             key={1}
             label="Remaining Payment:"
             content={<TextField value={shipment} label="Remaining cost" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="shipment"
               onBlur={sumOfCost}
                />}
            />
        <TwoItems
             key={1}
             label="Tax_Rate:"
             content={<TextField value={tax} label="tax" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="tax"
               onBlur={sumOfCost}
                />}
            />
        <TwoItems
             key={1}
             label="Delivery Cost:"
             content={<TextField value={delivery} label="delivery cost" style={{float:'right'}} id="standard-basic"
               onChange={change}
               name="delivery"
               onBlur={sumOfCost}
                />}
            /> */}
            {/* <TwoItems
             key={1}
             label="GRN Status:"
             content={<DropDown 
            floating="right"  
            title="Status"
            value={status_type}
            name="status_type"
            change={change}
            >
        <MenuItem value="pending">Pending</MenuItem>
        <MenuItem value="stored">Stored</MenuItem>
        
      </DropDown> } /> */}
              <TwoItems
             key={1}
             label="Additional Cost:"
             content={<div style={{float:'right'}}><ButtonComponent name="Add Cost" click={()=>addCost('total','total')} /></div>}  
            />
        <TwoItems
             key={1}
             label="Total Cost:"
              content={<p style={{float:'right'}}>{totalCost}</p>}
            />    
     </Paper>
    </div>
}

const getReceivedItems = (rows)=>{
    return  <TableContainer component={Paper}>
    <Table  size="small" aria-label="a dense table">
      <TableHead>
        <TableRow>
          <TableCell>Product</TableCell>
          <TableCell align="right">Barcode</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rows.map((row) => (
          <TableRow key={row.product_name}>
            <TableCell component="th" scope="row">
              {row.product_name}
            </TableCell>
            <TableCell align="right">{row.product_barcode}</TableCell>
          </TableRow>
        ))}
        <TableRow key={1}>
            <TableCell component="th" scope="row">
              total:
            </TableCell>
            <TableCell align="right">{rows.length}</TableCell>
          </TableRow>
      </TableBody>
    </Table>
  </TableContainer>
}

export const ButtonComponent = ({click,name}) =>{
    return <Button onClick={click} variant="contained" color="primary">
               {name} 
           </Button>
}

export const ShipmentCost =({change,shipCost,name,blur})=>{
 return <Grid container spacing={2}>
    <Grid item xs={6}><h3>Shipment Cost</h3></Grid>
    <Grid item xs={6}><TextField id="standard-basic" onBlur={blur} name={name} value={shipCost} onChange={change} label="Enter Cost" /></Grid>
  </Grid>
}
export const TaxCost =({change,taxCost,blur})=>{
  return <Grid container spacing={2}>
     <Grid item xs={6}><h3>Tax</h3></Grid>
     <Grid item xs={6}><TextField id="standard-basic" onBlur={blur} name="taxCost" value={taxCost} onChange={change} label="Enter Cost" /></Grid>
   </Grid>
 }
 export const OtherCost =({change,otherCost,blur})=>{
  return <Grid container spacing={2}>
     <Grid item xs={6}><h3>Other Cost</h3></Grid>
     <Grid item xs={6}><TextField id="standard-basic" onBlur={blur} name="otherCost" value={otherCost} onChange={change} label="Enter Cost" /></Grid>
   </Grid>
 }
 export const DeliveryCost =({change,deliveryCost,blur})=>{
  return <Grid container spacing={2}>
     <Grid item xs={6}><h3>Delivery Cost</h3></Grid>
     <Grid item xs={6}><TextField id="standard-basic" onBlur={blur} name="deliveryCost" value={deliveryCost} onChange={change} label="Enter Cost" /></Grid>
   </Grid>
 }


 export const RadioButton =({radioCheked,change,val})=>{
   console.log(radioCheked)
   return <Radio
   checked={radioCheked == val}
   onChange={change}
   value={val}
   name="radioCheked"
   inputProps={{ 'aria-label': 'A' }}
 /> 
 }

 export const Products =({edtqty,expandedItem,exapndId,SelecteedPO,addCost,selectId,change,editQty,editQtyName,setQty})=>{
  const classes = useStyles();
   return  <Paper style={{height:'auto',margin:'5px',padding:'5px'}} elevation={3}>
      <ExpansionPanel key={1} >
   <ExpansionPanelSummary
      style={{background:'#F2EEED',color:'black'}}
      aria-controls="panel2a-content"
      id="panel2a-header"
    >
       <Grid container spacing={2}>
          {/* <Grid item xs={1}><Typography ><b>Product:</b></Typography></Grid> */}
          <Grid item xs={5}><Typography >Product Name</Typography></Grid>
          <Grid item xs={1}><Typography ><b>Qty</b></Typography></Grid>
          {/* <Grid item xs={2}>{edtqty && <Typography >Edit qty</Typography>}</Grid> */}
          <Grid item xs={3}><Typography>
            View Items
          </Typography></Grid>
      </Grid>
    </ExpansionPanelSummary>
  </ExpansionPanel>
      {SelecteedPO.map(item=><ExpansionPanel
      
       expanded={item.product_name == exapndId}
      //  onChange={handleExpansion}
      key={item.product_name} >
        {console.log(item,'chekdtaof')}
   <ExpansionPanelSummary
      style={{background:'#F2EEED',color:'black',margin:'2px'}}
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel2a-content"
      id="panel2a-header"
    >
       <Grid container spacing={2}>
          {/* <Grid item xs={1}><Typography ><b>Product:</b></Typography></Grid> */}
          <Grid item xs={5}><Typography >{item.product_name}</Typography></Grid>
          <Grid item xs={2}><span style={{display:'inline-flex'}}>
          <form className={classes.rootText} noValidate autoComplete="off">
            <TextField
          style={{padding:0,margin:0}}
          id="standard-basic"
          onFocus={()=>selectId(item.product_name,item.uuid)}
          onChange={change}
          onBlur={()=>setQty(editQty <= item.items.length ? editQty : item.items.length)}
          name="editQty"
          value={item.product_name == editQtyName ? editQty: item.qty}
           /></form>
           <form className={classes.rootText} noValidate autoComplete="off">
            <TextField
          style={{padding:0,margin:0}}
          id="standard-basic"
         
          value={`/${item.count}`}
           /></form>
          </span></Grid>
          {/* <Grid item xs={2}>
            {edtqty && <Typography ><TextField id="standard-basic"
          onFocus={()=>selectId(item.product_name,item.uuid)}
          onChange={change}
          onBlur={()=>setQty(editQty <= item.items.length ? editQty : item.items.length)}
          name="editQty"
          value={item.product_name == editQtyName ? editQty: item.qty}
           /></Typography>}
           </Grid> */}
          <Grid item xs={3}><Typography>
          <Button style={{width:'210px'}} variant="contained" onClick={()=>expandedItem(item.product_name)} color="secondary">View Items</Button>
          </Typography></Grid>
      </Grid>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails style={{display:'block'}}>
       <TableContainer  component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow style={{background:'#318C9B'}}>
               <TableCell align="left">Item Name</TableCell>
                <TableCell align="left">Item Cost</TableCell>
                <TableCell align="left">Add Cost</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {console.log(item,'cehkItem')}
            {item.items.map((val,index) => { return index+1 <= item.qty && <TableRow 
            //  onClick={()=>getOneProduct(item,name)}
             style={{background:'rgb(215, 222, 221)',marginTop:'2px'}} button key={val.product_uuid}>
              <TableCell align="left" >{val.product_item}</TableCell>
              <TableCell align="left">{val.product_item_cost}</TableCell>
              <TableCell align="left" ><ButtonComponent name="Add Cost" click={()=>addCost(val.product_uuid,item.product_name)}  /></TableCell>
            </TableRow>
            })}
              </TableBody>
              </Table>
              </TableContainer>
    </ExpansionPanelDetails>
  </ExpansionPanel>)}
  </Paper>
 }