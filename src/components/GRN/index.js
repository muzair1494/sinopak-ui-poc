import React,{Component} from 'react'
import {ItemList,ButtonComponent, ItemDetail, ExpansionPanelGRN, ModalCost,DeliveryCost, OtherCost,ShipmentCost,TaxCost} from './propsFunction'
import { connect } from 'react-redux';
import { getGrnList } from './../../actions/simpleAction'
import FontAwesome from 'react-fontawesome'
import {GetSearchData} from './../../common/searchINput'

class GRN extends Component{

    constructor(props){
        super(props)
        this.state={
            items:[1,2,3,4,5,6],
            checked:[],
            pending:true,
            grn:[],
            selectId:'',
            selectId2:'',
            selectId3:'',
            shipmentCost:0,
            other:0,
            commentItem:'',
            search:'',
            openForm:false,
            comment:"",
            grn_no:"",
            shipment:0,
            tax:0,
            delivery:0,
            totalCost:0,
            username:"",
            entryDate:"",
            grn_date:"",
            sysId:"abcd1#2345",
            itemsCost:0,
            filteredItems:[],
            mySet:[],
            openMod:false,
            costType:'',
            CostId:'',
            newItem:[],
            deliveryCost:0,
            otherCost:0,
            taxCost:0,
            shipCost:0,
            costArr:[],
            enterCosts:[],
            prodId:'',
            searchpo:'',
            data:[],
            id:'',
            uuid:'',
            getUuidItem:'',
            entry_date:"",
            receive_person:"",
            adv_pay:"",
            seal_no:"",
            order_date:"",
            order_items:"",
            status_type:"",
            chekcedId:[]
        }
    }
    componentDidMount(){
        this.props.getGrnList()
        this.getCurrentDateTime()
        this.setCostType()
    }
    componentWillReceiveProps(nextprops){
      if(this.props.grn != nextprops.grn){
        this.setState({pending:nextprops.pending})
          let arr=[]
          let arr2=[]
          for(var i=0;i<nextprops.grn.length;i++){
            if(nextprops.grn[i].type == 'purchased'){ 
            arr2.push(nextprops.grn[i].vendor_name)
            arr.push(nextprops.grn[i])}
          }
          let set = new Set(arr2)
          var mySet = Array.from(set)  
          this.setState({grn:arr,mySet},()=>{
          for(var i=0 ;i<this.state.grn.length ;i++ ){
          this.state.grn[i].item.map(item=>item.count= item.product_purchase_cost)
        }
        this.setState({grn: this.state.grn})
        console.log(this.state.grn,'thisgrn')
        })
      }
    }
    setCostType=()=>{
      
      let arr = [
        {
        key:"shipment cost" },
        {
          key:"tax"},
        {
          key:"delivery cost" },
        {
          key:"other cost"
        }
      ]
      this.setState({costArr:arr})
    }
    getCurrentDateTime=()=>{
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        this.setState({entryDate:dateTime})
    }
    handleChange=(e)=>{
        this.setState({[e.target.name]: e.target.value,open:false})
    }
    handleToggle = (value,id) => () => {
        const {checked, chekcedId} = this.state
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        const currentIndex2 = chekcedId.indexOf(id);
        const newChecked2 = [...chekcedId];
    
        if (currentIndex === -1) {
          newChecked.push(value);
        } else {
          newChecked.splice(currentIndex, 1);
        }

        if (currentIndex2 === -1) {
          newChecked2.push(id);
        }
        this.setState({checked:newChecked,chekcedId:newChecked2,id},()=>{
          console.log(this.state.checked,this.state.chekcedId)
        });
      };
      selectId=(id,cost,key)=>{
          if(key== 'ship'){
        this.setState({selectId:id,shipmentCost:cost})}
        if (key == "other"){this.setState({selectId2:id,other:cost})}
        if (key == "comment"){this.setState({selectId3:id,commentItem:cost})}
      }
      setCost=(cost,key)=>{
        if (key == "ship"){
        this.setState({
            grn: this.state.grn.map((item)=>{
                if(item.product_barcode === this.state.selectId){
                  item.product_purchase_cost = cost
                }
                return item
            })  
          })}
          if (key == "other"){
            this.setState({
                grn: this.state.grn.map((item)=>{
                    if(item.product_barcode === this.state.selectId2){
                      item.cost.othercost = cost
                    }
                    return item
                })  
              })  
          }
          if (key == "comment"){
            this.setState({
                grn: this.state.grn.map((items)=>{
                    if(items.uuid === this.state.selectId3){
                      items.item.comment = cost
                    }
                    return items
                })  
              })  
          }
      }
      selectedItems=()=>{
          this.setState({itemsCost:0,filteredItems:[]},()=>{
            const {checked,grn,id} = this.state
            let findId = grn.find(item=> item.uuid== id)
            let filteredItems = findId.item.filter((item)=>{
                return checked.includes(item.product_barcode)== true ? item : null
             })
             this.setState({openForm:true,filteredItems},()=>{
                //  let itemsCost = filteredItems.reduce(function(sum, number){
                //    // no need for += anymore due to sum
                //    return sum=sum + Number(number.cost.shipment_cost) + Number(number.cost.othercost)
                //  }, 0)
                // this.state.filteredItems.map(item=>item.count= item.product_purchase_cost)
                let itemsCost = filteredItems.reduce(function(sum, number){
                  // no need for += anymore due to sum
                  return sum=sum + Number(number.count)
                }, 0)
                console.log(this.state.filteredItems,'this.state.filteredItems')
                 this.setState({itemsCost},()=>this.sumOfCost())
             })

          })
      }
      sumOfCost=()=>{
        const { shipment,tax, delivery, itemsCost } = this.state
        this.setState({totalCost:Number(shipment)+Number(tax)+Number(delivery)+Number(itemsCost)},()=>{
          let y = Number(this.state.adv_pay)/100
          let x = Number(this.state.totalCost) * y
          console.log(x,'totalmultiply')
          let z = Number(this.state.totalCost) - x 
          console.log(z,'totalcostssss')
          this.setState({totalCost:z})
        })
      }
      handleCapture = (e) => {
        const fileReader = new FileReader();
        const files = e.target.files
        console.log(files,'files')
        if(files[0].type == "application/pdf"){
        fileReader.readAsDataURL(files[0]);
        fileReader.onload = (e) => {
            //const apiURL = "http//..."
            //const formatFileData = {file: e.target.result}
            //at the top import libraray
            //import axios ,{post} from 'axios'
            //return post(apiURL,formatFileData)
            //.then(res=>..)
            // console.log(e.target.result,'target.accept')
        }
        // fileReader.onload = (e) => {
        //     this.setState((prevState) => ({
        //         [name]: [...prevState[name], e.target.result]
        //     }));
        // };
    }else{alert('please insert only pdf file')}
    };
    dispComp =()=>{
        this.setState({openForm:false})
    }
    postData=()=>{
        const {comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId} = this.state
        const values = {comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId}
        console.log(values,'values')
    }
    addCost =(id,uuid)=>{
      const {grn} = this.state
      // let values = grn.find(item=>item.product_barcode === id)
      this.setState({openMod:true,uuid,prodId:id},()=>{
        let item = this.state.grn.find(uid=>uid.uuid == uuid)
        this.setState({getUuidItem:item,order_date:item.ordered_date,order_items:item.item.length,adv_pay:item.payment_advance,seal_no: item.seal_no})
      })
    }
    cancelSubmit =()=>{
      this.setState({openMod:false})
    }
    submit=()=>{
      this.sumOfpurchaseCost()
      this.setState({openMod:false,enterCosts:[]})
    }
    addNewItem=(type)=>{
      const { costArr, enterCosts } = this.state
      this.setState({enterCosts:[]},()=>{
        let getData =  costArr.find(item=> item.key === type)
        this.setState({enterCosts:[...enterCosts,getData]})  
      })
      }
    sumOfpurchaseCost=()=>{
      const { deliveryCost,otherCost,taxCost,shipCost, prodId ,uuid,getUuidItem } = this.state
      this.setState({grn: this.state.grn.map(items=>{
        if(uuid == items.uuid){
          items.item.map((child)=>{
          if(child.product_barcode == prodId ){
            child.count = Number(child.count) + Number(deliveryCost)+Number(otherCost)+Number(taxCost)+Number(shipCost)
          }
          return child
        })
      }
        return items
      })},()=>{
        this.setState({deliveryCost:0,
          otherCost:0,
          taxCost:0,
          shipCost:0})
      })
    }
    getOneData=()=>{
      this.setState({data:[]},()=>{
      if(this.state.searchpo != ''){
      const {searchpo, grn}  = this.state
      let data = grn.filter((x)=> {return searchpo==x.purchase_order_no})
      let arr = []
      arr.push(data)
      this.setState({data:arr},()=>console.log(this.state.data))
      }
    })
      
    }
    render(){
        const {status_type,order_date,order_items,adv_pay,seal_no,receive_person,entry_date,data,otherCost,taxCost,deliveryCost,shipCost,enterCosts,CostId,costType,openMod,mySet,pending,selectId2,selectId3,filteredItems,grn,shipmentCost,other,commentItem,selectId,search,openForm} = this.state
        const {searchpo,chekcedId,checked,comment,grn_no,grn_date,shipment,tax,delivery,totalCost,username,entryDate,sysId
        } = this.state
        // let filteredData = grn.filter(items => {
        //     return (items.item.product_name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1
        //     || items.vendor_name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1
        //     );
        // })
        let filteredSetData = mySet.filter(items => {
          return (items.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1);
        })
        let filteredpurchaseOrder = grn.filter(items => {
          return (items.purchase_order_no.toString().toLowerCase().indexOf(searchpo.toString().toLowerCase()) !== -1
          );
        })
        return(<div >
              {pending && <h3>Loading...</h3>}
              {!openForm && !pending && <div style={{display:'flex',justifyContent:'center',width:'38%',marginTop:'5px'}}>
               <GetSearchData name="search"
                        search={search} 
                        getSearched={this.handleChange}
                        title="Vendor Search" />
                <GetSearchData name="searchpo"
                        onBlur={this.getOneData}
                        search={searchpo} 
                        getSearched={this.handleChange}
                        title="purchase order no. Search" />
                   </div>}
                   
                {!openForm && !pending && 
                // <div style={{display:'flex',justifyContent:'center',marginTop:'10px'}}>
                <ExpansionPanelGRN 
                    items={filteredpurchaseOrder}
                    mySet={filteredSetData}
                    checked={checked}
                    chekcedId={chekcedId}
                    handleToggle={this.handleToggle}
                    addCost={this.addCost} 
                    data={data}
                   />
                }
             {/* <ItemList 
                selectId={this.selectId}
                checked={checked} handleToggle={this.handleToggle}
                change={this.handleChange}
                editCost={shipmentCost}
                editCost2={other}
                editCost3={commentItem}
                name="shipmentCost"
                name2="other"
                editId={selectId}
                editId2={selectId2}
                editId3={selectId3}
                items={filteredData}
                setCost={this.setCost}
                /> */}
            {/* // </div> */}
            {!openForm && !pending && <div style={{display:'flex',justifyContent:'center',width:'5%',marginTop:'5px'}}>
                <ButtonComponent
                click={this.selectedItems}
                name="Submit"
                />
            </div>}
            {openForm && <div style={{display:'flex',justifyContent:'center',width:'64%',marginTop:'5px'}}>
            <FontAwesome
                    className="super-crazy-colors"
                    name="arrow-left"
                    onClick={this.dispComp}
                    size="2x"
                    // spin
                    style={{marginTop:'17px',color:"blue" ,textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                    />
                    <h2 style={{marginLeft:'10px'}}>Detail</h2>
            </div>}
            {openForm && <div style={{display:'flex',justifyContent:'center',marginTop:'10px'}}>
                <ItemDetail
                status_type={status_type}
                comment={comment}
                grn_no={grn_no}
                shipment={shipment}
                tax={tax}
                delivery={delivery}
                totalCost={totalCost}
                username={username}
                entryDate={entryDate}
                sysId={sysId}
                grn_date={grn_date}
                receive_person={receive_person}
                entry_date={entry_date}
                change={this.handleChange}
                sumOfCost={this.sumOfCost}
                handleCapture={this.handleCapture}
                receivedItems={filteredItems}
                adv_pay={adv_pay}
                seal_no={seal_no}
                order_date={order_date}
                order_items={order_items}
                />
            </div> }
            {openForm && <div style={{display:'flex',justifyContent:'center',width:'68%',marginTop:'5px'}}>
                <ButtonComponent
                click={this.postData}
                name="Confirm"
                />
            </div>}
            <ModalCost
            submit={this.submit}
            enterCosts={enterCosts}
            otherCost={otherCost}
            taxCost={taxCost} 
            deliveryCost={deliveryCost}
            shipCost={shipCost}
            addNewItem={this.addNewItem} cancel={this.cancelSubmit} open={openMod} costType={costType} change={this.handleChange} />
        </div>)
    }
}


const mapDispatchToProps = dispatch => ({
    getGrnList: () => dispatch(getGrnList())
   })
const mapStateToProps = state => ({
    grn: state.simpleReducer.GRN_List,
    pending: state.simpleReducer.pending_GRN,
   })

export default connect(mapStateToProps, mapDispatchToProps)(GRN)