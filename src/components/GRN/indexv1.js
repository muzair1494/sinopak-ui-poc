import React,{Component} from 'react'
import {RadioButton,Products,ItemList,ButtonComponent, ItemDetail, ExpansionPanelGRN, ModalCost,DeliveryCost, OtherCost,ShipmentCost,TaxCost} from './propsFunction'
import { connect } from 'react-redux';
import { getGrnList } from './../../actions/simpleAction'
import FontAwesome from 'react-fontawesome'
import {GetSearchData} from './../../common/searchINput'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

class GRN extends Component{
    state={
        grn:[],
        mySet:[],
        vendor:[],
        PO:[],
        SelecteedPO:[],
        enterCosts:[],
        costArr:[],
        pending:true,
        openNewComponent:false,
        grnForm:false,
        radioCheked:'',
        entryDate:'',
        search:'',
        vendorSearch:'',
        POSearch:'',
        searchPO:'',
        costType:'',
        itemId:'',
        prodName:'',
        editQty:'',
        editQtyName:'',
        seal_no:'',
        ship_channel:'',
        imported_location:'',
        receive_person:'',
        receiving_date:'',
        order_items:'',
        comment:'',
        status_type:'',
        ordered_date:"",
        payment_advance:"",
        receivedItems:0,
        deliveryCost:0,
        otherCost:0,
        taxCost:0,
        shipCost:0,
        sumOfCost:0,
        sumCosts:0,
        exapndId:'',
        searchItem:'',
        findOneData:{},
        tempSelectedPO:[],
        edtqty:true,
        tot:false

    }
    componentDidMount(){
        this.props.getGrnList()
        this.getCurrentDateTime()
        this.setCostType()
    }
    componentWillReceiveProps(nextprops){
        if(this.props.grn != nextprops.grn){
          this.setState({pending:nextprops.pending})
            let arr=[]
            let arr2=[]
            let arr3=[]
            for(var i=0;i<nextprops.grn.length;i++){
              if(nextprops.grn[i].type == 'purchased'){ 
              arr2.push(nextprops.grn[i].vendor_name)
              arr3.push(nextprops.grn[i].purchase_order_no)
              arr.push(nextprops.grn[i])}
            }
            let set = new Set(arr2)
            var mySet = Array.from(set)  
            this.setState({grn:arr,mySet,vendor:mySet,PO:arr3},()=>{
          console.log(this.state.grn,this.state.mySet,'thisgrn')
          })
        }
      }
      handleChange=(e)=>{
        this.setState({[e.target.name]: e.target.value})
    }
    handleChange2=(e)=>{
        this.setState({[e.target.name]: e.target.value},()=>{
            this.createGRN(this.state.radioCheked)
        })
    }
      getCurrentDateTime=()=>{
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        this.setState({entryDate:dateTime})
    }
    getCurrentData=(data,key)=>{
        if(key === 'vendor'){

        }
    }
    getBarcodeSearch=()=>{
        var arr=[] 
        var qtys = 0
        console.log(this.state.SelecteedPO,'this.state.SelecteedPO')
        let sumUP = this.state.SelecteedPO.map((item)=>{
            item.items.map((child,index)=>{
                if(child.product_barcode == this.state.searchItem){
                    console.log(this.state.searchItem)
                    qtys = item.items.length
                    arr.push(child)
                }
                return child
            })
            return item
        })
        if(arr.length > 0){
        this.setState({edtqty:false,tempSelectedPO:this.state.SelecteedPO},()=>{
            var values ={
                product_name: arr[0].product_name,
                items: arr ,
                qty: qtys
            }
            let arr2 = []
            arr2.push(values)
            this.setState({SelecteedPO:arr2,exapndId:arr[0].product_name})
        })}else{
            this.setState({edtqty:true,SelecteedPO: this.state.tempSelectedPO})
        }
        console.log(arr,'sumUP')

    }
    getSearchedData=(val,key)=>{
        const {search, searchPO, grn} = this.state
        if(key == 'selctedGRN'){
            this.getBarcodeSearch()
        }
        if(search != '' && searchPO != ''){
            console.log('vendor and po not null')
            const {grn, vendorSearch, search} = this.state
            let findData = grn.filter(item=>item.vendor_name === search)
            let arr=[]
            for(var i=0;i<findData.length;i++){
                arr.push(findData[i].purchase_order_no)
            }
            if(arr.length > 0){
                this.setState({vendorSearch:search,PO:[]},()=>{
                this.setState({PO:arr})
            })
            }
            const { POSearch} = this.state
            let arr2=[]
            let findData2 = grn.find(item=>item.purchase_order_no === searchPO)
            if(findData2 != undefined){
                arr2.push(findData2.vendor_name)
            }
            console.log(findData2,arr,'chekc')
            if(arr.length > 0){
                this.setState({POSearch:searchPO,vendor:[]},()=>{
                    console.log(arr2)
                    this.setState({vendor:arr2})
                })
            }else{
                this.getAllData('vendor')
            }
        }
        else 
        if(key == "VE" && search != '' && searchPO == ""){
            console.log('2 venodr not null and po null')
                const {grn, vendorSearch, search} = this.state
                let findData = grn.filter(item=>item.vendor_name === search)
                let arr=[]
                for(var i=0;i<findData.length;i++){
                    arr.push(findData[i].purchase_order_no)
                }
                console.log(arr,findData,'vedorsSearch')
                if(arr.length > 0){
                    this.setState({vendorSearch:search,PO:[]},()=>{
                        
                    this.setState({PO:arr})
                })
                }else{
                     this.getAllData('vendor')
                }
                
        }
        else if(key == "PO" && searchPO != '' && search == ""){
            console.log('3')
                const {grn, POSearch} = this.state
                let arr=[]
                let findData = grn.find(item=>item.purchase_order_no === searchPO)
                if(findData != undefined){
                    arr.push(findData.vendor_name)
                }
                console.log(findData,arr,'chekc')
                if(arr.length > 0){
                    this.setState({POSearch:searchPO,vendor:[]},()=>{
                        
                        this.setState({vendor:arr})
                    })
                }else{
                    this.getAllData('vendor')
                }
        }else if(searchPO === '' && search === '') {
            console.log('4')
            this.getAllData('vendor')
        }
        else {
            if(search != "" && searchPO == ""){
                console.log('5 again vendor not null but po null')
                const {grn, vendorSearch, search} = this.state
                let findData = grn.filter(item=>item.vendor_name === search)
                let arr=[]
                for(var i=0;i<findData.length;i++){
                    arr.push(findData[i].purchase_order_no)
                }
                console.log(arr,findData,'vedorsSearch')
                if(arr.length > 0){
                    
                    this.setState({vendorSearch:search,PO:[]},()=>{
                        
                    this.setState({PO:arr},()=>{
                        console.log(this.state.PO,'chekdata')
                    })
                })
                }else{
                     this.getAllData('vendor')
                }
        
        }
            else if(search == "" && searchPO != ""){
                console.log('6')
            this.getAllData('pos')}
        }
    }
    getAllData=(key)=>{
        const {search, searchPO} = this.state
        let arr2 =[]
        if(key === 'vendor' || key === "vendors"){
        for(var i=0;i<this.state.grn.length;i++){
            arr2.push(this.state.grn[i].vendor_name)
        }
        this.setState({vendor:arr2,vendorSearch:search})
        }
        let arr=[]
        if(key === 'vendor' || key === "pos"){
        for(var i=0;i<this.state.grn.length;i++){
            arr.push(this.state.grn[i].purchase_order_no)
        }
        this.setState({PO:arr,POSearch:searchPO})
        }
    }
    createGRN=(value)=>{
        const {grn} = this.state
        let findOneData =  grn.find(item=>item.purchase_order_no == value)
        this.setState({findOneData,payment_advance:findOneData.payment_advance,ordered_date:findOneData.ordered_date})
        let setArr=[]
        for(var i=0 ;i< findOneData.item.length;i++){
            setArr.push(findOneData.item[i].product_name)
        }
        let set = new Set(setArr)
        var mySet = Array.from(set) 
        var arr2 =[]
        for(var i=0 ;i< mySet.length;i++){
            var array =[]
            let baseValue={
                product_name: mySet[i]
            }
            var count =0
            for(var j=0 ;j<findOneData.item.length;j++){
                
                if(mySet[i] == findOneData.item[j].product_name){
                    let values={
                        product_uuid: findOneData.item[j].uuid,
                        product_name: findOneData.item[j].product_name, 
                        product_image: findOneData.item[j].product_image,
                        product_barcode: findOneData.item[j].product_barcode,
                        product_category: findOneData.item[j].product_category,
                        product_description:findOneData.item[j].product_description,
                        product_weight:findOneData.item[j].product_weight,
                        product_item: findOneData.item[j].product_item,
                        product_item_cost : findOneData.item[j].product_item_cost,
                    }
                    array.push(values)
                    count++
                }
            }
            baseValue.items = array
            baseValue.qty= count
            baseValue.count= count
            arr2.push(baseValue)
        }
        this.setState({SelecteedPO:arr2,tempSelectedPO:arr2,openNewComponent:true},()=>{
            console.log(this.state.SelecteedPO,this.state.findOneData,'chekData')
        })
    }
    selectId=(prodName,id)=>{
        const {SelecteedPO} = this.state
        let findData = SelecteedPO.find(item=>item.product_name == prodName)
        this.setState({editQtyName:prodName,editQty:findData.qty})
    }
    expandedItem= (id)=>{
        this.setState({exapndId:id})
    }
    setQty=(qty)=>{
        const {editQtyName} = this.state
        this.setState({
            SelecteedPO: this.state.SelecteedPO.map((item)=>{
                if(item.product_name == editQtyName){
                    item.qty = qty
                }
                return item
            })
        },()=>{
           this.getTotCost()
        })
    }
    getTotCost =()=>{
        var sumqty=0 
        var qtys = 0
        var sums=0 
        let sumUP = this.state.SelecteedPO.map((item)=>{
            sumqty = sumqty + Number(item.qty)
            qtys = qtys + item.items.length
            item.items.map((child,index)=>{
                if(index+1 <= item.qty){
                sums = sums  + Number(child.product_item_cost)}
            })
        })
    console.log(sums,'cheksums')
      this.setState({sumCosts: sums,order_items:qtys,receivedItems:sumqty})
    }
    setCostType=()=>{
      let arr = [
          {
          key:"shipment cost" },
          {
            key:"tax"},
          {
            key:"delivery cost" },
          {
            key:"other cost"
          }
        ]
        this.setState({costArr:arr})
      }
    addCost=(id,prodName)=>{
        this.setState({openMod:true,itemId:id,prodName})
    }
    addNewItem=(type)=>{
        const { costArr, enterCosts } = this.state
        this.setState({enterCosts:[]},()=>{
          let getData =  costArr.find(item=> item.key === type)
          this.setState({enterCosts:[...enterCosts,getData]})  
        })
    }
    submit =()=>{
        const {itemId,prodName,shipCost,deliveryCost,taxCost,otherCost} = this.state
        if(this.state.tot){
            this.setState({sumCosts: Number(this.state.sumCosts) + Number(shipCost)+Number(taxCost)+Number(deliveryCost)+Number(otherCost)})
            }else{
            
            this.setState({
            SelecteedPO: this.state.SelecteedPO.map((item,index)=>{
                if(item.product_name == prodName){
                    item.items.map((child)=>{
                       if(child.product_uuid == itemId){
                           child.product_item_cost = Number(child.product_item_cost) + Number(shipCost)+Number(taxCost)+Number(deliveryCost)+Number(otherCost)
                       }
                       return child
                    })
                }
                return item
            })
        },()=>this.getTotCost())}
        this.setState({openMod:false,tot:false,enterCosts:[], deliveryCost:0,
            otherCost:0,
            taxCost:0,
            shipCost:0,})
    }
    cancelSubmit =()=>{
        this.setState({openMod:false,tot:false})
    }
    selectedItems=()=>{
        this.getTotCost()
        this.setState({grnForm:true})
        // console.log(sum, SelecteedPO)
    }
    addtotalCost=(a,b)=>{
        this.setState({openMod:true,tot:true})
    }
    ConfirmItems=()=>{}
render(){
    //items data and  data variables
    const {exapndId,sumCosts,mySet,grn,payment_advance,receivedItems,ordered_date,sumOfCost,order_items,status_type,comment,receive_person,receiving_date,entryDate,seal_no,ship_channel,imported_location, editQty,editQtyName, SelecteedPO, costType ,enterCosts,otherCost,taxCost,deliveryCost,shipCost} = this.state

    //search variables
    const {searchItem,search,vendorSearch,POSearch,searchPO} = this.state

    //comparing variables
    const { vendor,PO , radioCheked } = this.state

    //boolean values
    const {edtqty, openNewComponent, openMod, grnForm} = this.state

    let filteredSetData = mySet.filter(items => {
        return (items.toString().toLowerCase().indexOf(vendorSearch.toString().toLowerCase()) !== -1);
    })  
    let filteredGRNData = grn.filter(items => {
        return (items.purchase_order_no.toString().toLowerCase().indexOf(POSearch.toString().toLowerCase()) !== -1);
    })  
    let filteredselectedData = SelecteedPO.filter(items => {
        return (items.product_name.toString().toLowerCase().indexOf(searchItem.toString().toLowerCase()) !== -1);
    })  
    return(
        <div>
            <Grid container spacing={2}>
               {!openNewComponent && <Grid item xs={6}>
                    <GetSearchData name="search"
                        val="VE"
                        onBlur={this.getSearchedData}
                        search={search} 
                        getSearched={this.handleChange}
                        title="Vendor Search" />
                    <Paper style={{height:'400px',margin:'5px',padding:'5px'}} elevation={3}>
                        {filteredSetData.map((item)=>{return vendor.includes(item) &&  <Paper style={{margin:'5px',padding:'5px'}} elevation={3} key={item} onClick={()=>this.getCurrentData(item,'vendor')} >{item}</Paper>})}
                    </Paper>
                </Grid>}
                {openNewComponent && <Grid item xs={6}>
                <h3>Product List</h3>   
                <GetSearchData name="searchItem"
                    val="selctedGRN"
                    onBlur={this.getSearchedData}
                    search={searchItem} 
                    getSearched={this.handleChange}
                    title="#Item Barcode" /> 
                <Products 
                edtqty={edtqty}
                expandedItem={this.expandedItem}
                exapndId={exapndId}
                setQty={this.setQty}
                selectId={this.selectId}
                change={this.handleChange}
                editQty={editQty}
                editQtyName={editQtyName}
                addCost={this.addCost} SelecteedPO={SelecteedPO}/>
                 <ButtonComponent
                click={this.selectedItems}
                name="Refresh"
                />
                </Grid>}
                {openNewComponent && grnForm && <Grid item xs={6}>
                <h3>GRN FORM</h3>
                <ItemDetail
                addCost={this.addtotalCost}
                ordered_date={ordered_date}
                status_type={status_type}
                comment={comment}
                grn_no='dsadsd'
                shipment={ship_channel}
                tax="asd"
                delivery="sad"
                totalCost={sumCosts}
                username={5432}
                entryDate={entryDate}
                sysId={123}
                grn_date={receiving_date}
                receive_person={receive_person}
                entry_date='12-2-2020'
                change={this.handleChange}
                sumOfCost={this.sumOfCost}
                handleCapture={this.handleCapture}
                receivedItems={receivedItems}
                adv_pay={payment_advance}
                seal_no={seal_no}
                order_date="12-3-2020"
                order_items={order_items}
                imported_location={imported_location}
                />
                <ButtonComponent
                click={this.ConfirmItems}
                name="Confirm"
                />
                    </Grid>}
                {!openNewComponent && <Grid item xs={6}>
                    <GetSearchData name="searchPO"
                        val="PO"
                        onBlur={this.getSearchedData}
                        search={searchPO} 
                        getSearched={this.handleChange}
                        title="Purchase Order Search" />
                    <Paper style={{height:'400px',margin:'5px',padding:'5px'}} elevation={3}>
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Purchase Order</TableCell>
                                    <TableCell align="left">Qty</TableCell>
                                    <TableCell align="left">Order Date</TableCell>
                                    <TableCell align="left">Select</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {filteredGRNData.map((item)=>{return PO.includes(item.purchase_order_no) &&  <TableRow key={item.purchase_order_no} onClick={()=>this.getCurrentData(item,'purchaseOrders')} style={{margin:'5px',padding:'5px'}}>
                                    <TableCell>{item.purchase_order_no}</TableCell>
                                    <TableCell align="left">{item.item.length}</TableCell>
                                    <TableCell align="left">{item.ordered_date}</TableCell>
                                    <TableCell align="left"><RadioButton val={item.purchase_order_no} radioCheked={radioCheked} change={this.handleChange2}/></TableCell>
                                </TableRow>
                            })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    </Paper>    
                </Grid>}
            </Grid>
            <ModalCost
            submit={this.submit}
            enterCosts={enterCosts}
            otherCost={otherCost}
            taxCost={taxCost} 
            deliveryCost={deliveryCost}
            shipCost={shipCost}
            addNewItem={this.addNewItem} cancel={this.cancelSubmit} open={openMod} costType={costType} change={this.handleChange} />
        </div>
    )
}
}


const mapDispatchToProps = dispatch => ({
    getGrnList: () => dispatch(getGrnList())
   })
const mapStateToProps = state => ({
    grn: state.simpleReducer.GRN_List,
    pending: state.simpleReducer.pending_GRN,
   })

export default connect(mapStateToProps, mapDispatchToProps)(GRN)