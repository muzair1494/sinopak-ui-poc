import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';



export const SinoBatchProducts =({batchProducts})=>{return <TableContainer component={Paper}>
<Table  aria-label="simple table">
    <TableHead>
        <TableRow>
            <TableCell>uuid</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Image</TableCell>
            <TableCell>Category</TableCell>
            <TableCell>Shape</TableCell>
            <TableCell>Weight</TableCell>
            <TableCell>Dimentsions</TableCell>
            <TableCell>Color</TableCell>
            <TableCell>Qty</TableCell>
            <TableCell>Cost</TableCell>
        </TableRow>
    </TableHead>
    <TableBody>
        {batchProducts.map((item) => (
            <TableRow key={item.uuid}>
                <TableCell component="th" scope="row">{item.uuid}</TableCell>
                <TableCell>{item.name}</TableCell>
                <TableCell>{item.image}</TableCell>
                <TableCell>{item.category}</TableCell>
                <TableCell>{item.attributes.shape}</TableCell>
                <TableCell>{item.attributes.wieght}</TableCell>
                <TableCell>{item.attributes.dimensions}</TableCell>
                <TableCell>{item.attributes.color}</TableCell>
                <TableCell>{item.qty}</TableCell>
                <TableCell>{item.cost}</TableCell>
            </TableRow>
        ))}
    </TableBody>
</Table>
</TableContainer>}




export const SinoBatchProducts2 =({batchProducts,getcalled})=>{return <TableContainer component={Paper}>
<Table  aria-label="simple table">
    <TableHead>
        <TableRow>
            <TableCell>uuid</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Image</TableCell>
            {/* <TableCell>Category</TableCell> */}
            <TableCell>Shape</TableCell>
            <TableCell>Weight</TableCell>
            <TableCell>Dimentsions</TableCell>
            <TableCell>Color</TableCell>
            <TableCell>Qty</TableCell>
            <TableCell>Cost</TableCell>
            <TableCell>View</TableCell>
        </TableRow>
    </TableHead>
    <TableBody>
        {batchProducts.map((item) => (
            <TableRow key={item.uuid}>
                <TableCell component="th" scope="row">{item.uuid}</TableCell>
                <TableCell>{item.name}</TableCell>
                <TableCell>{item.image}</TableCell>
                {/* <TableCell>{item.category}</TableCell> */}
                <TableCell>{item.attributes.shape}</TableCell>
                <TableCell>{item.attributes.wieght}</TableCell>
                <TableCell>{item.attributes.dimensions}</TableCell>
                <TableCell>{item.attributes.color}</TableCell>
                <TableCell>{item.qty}</TableCell>
                <TableCell>{item.cost}</TableCell>
                <TableCell><button onClick={()=>getcalled({product_id:item.uuid,vendor_id:item.vendor_id})}>View Details</button></TableCell>
            </TableRow>
        ))}
    </TableBody>
</Table>
</TableContainer>}

