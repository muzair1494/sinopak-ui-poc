import React, {Component} from 'react'
import axios from 'axios';
import {SinoBatchProducts2} from './propsFunctions'
import {TransitionsModal} from './../../common/modal'
import {Title} from './../list/propslist' 
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import TextField from '@material-ui/core/TextField';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export default class Product extends Component{
    constructor(props){
        super(props);
        this.state={
            products:[],
            inventory:[],
            vendors:[],
            detail:[],
            detailProd:{},
            open:false,
            search:''
        }
    }
    
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/sino_products').then((res) => {
            this.setState({ products: res.data.products, loading: false })
            console.log(this.state.products)
        })
        axios.get('http://demo1951402.mockable.io/sino_inventory').then((res) => {
            this.setState({ inventory: res.data.invemntory})
        })
        axios.get('http://demo1951402.mockable.io/vendors').then((res) => {
            this.setState({ vendors: res.data.vendors })
        })
    }
    buttonCalled=(value)=>{
        console.log(value)
        this.getInventory(value)
    }

    getInventory =(value)=>{
        const {inventory,vendors,products} = this.state
        let filteredProducts = products.find((item)=>{
            return value.product_id == item.uuid
        })
        const filteredInventoryData = inventory.filter((item)=>{
            return value.product_id == item.prod_id
        })
        const filteredVendorData = vendors.find((item)=>{
            return value.vendor_id == item.uuid
        })
        let finalData = filteredVendorData != undefined && filteredInventoryData.map(option => {
            // New properties to be added
            const newPropsObj = {
                vendor_name: filteredVendorData.name,
                 vendor_cost: filteredVendorData.cost
            };
          
            // Assign new properties and return
            return Object.assign(option, newPropsObj);
          });
          console.log(filteredProducts)
        this.setState({detail:finalData,detailProd:filteredProducts},()=>this.handleOpen())
    }
    

     handleOpen = () => {
      this.setState({open:true})
      };
    
       handleClose = () => {
        this.setState({open:false})
      };
      updateSearch = e => {
        this.setState({ search:e.target.value },()=>console.log(this.state.search,'search'));
    };

render(){
    let {search,open,products,detail,detailProd} = this.state
    let filteredData = products.filter(items => {
        return (items.name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) !== -1);
    })
    return(<div>
        <Title listTitle="Batch List" />
        
        {/* search bars */}
        <Grid container spacing={2}>
        <Grid item xs={2}>
        <form style={{  margin: '5px',
                width: '25ch'}} noValidate autoComplete="off">
            <TextField id="standard-basic" label="search" onChange={this.updateSearch}/>
        </form>
        </Grid>
        <Grid item xs={2}>
        <FormControl style={{margin: '5px',minWidth: 220}}>
        <InputLabel id="demo-simple-select-label">search by category</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={34}
        //   onChange={handleChange}
        >
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>
        </Grid>
        </Grid>
        

        {/* display list of products */}
        <SinoBatchProducts2 batchProducts={filteredData} getcalled={this.buttonCalled}/>
        
        {/* detail products with inventory */}
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          style={{  display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',}}
          open={open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div style={{backgroundColor: "white",
                        border: '2px solid #000',
                        padding:'10px',
                        width:'50%',height:'auto',overflow:'hidden'}}>
        <h3>Detail</h3>
        <Paper elevation={3} >
        <Grid container spacing={3}>
        <Grid item xs={3}>
        <h2 id="transition-modal-title">{detailProd.image}</h2>
        </Grid>
        <Grid item xs={9}>
          <ul>
              <li>{detailProd.name}</li>
              <li>{detailProd.cost}</li>
          </ul>
        </Grid>
        </Grid>
        </Paper>

        <Table  aria-label="simple table">
    <TableHead>
        <TableRow>
            <TableCell>Barcode</TableCell>
            <TableCell>Vendor</TableCell>
            <TableCell>Vendor Cost</TableCell>
            <TableCell>Other Cost</TableCell>
            <TableCell>Documents</TableCell>
        </TableRow>
    </TableHead>
    <TableBody>
        {detail.length>0 && detail.map((item) => (
            <TableRow key={item.uuid}>
                <TableCell component="th" scope="row">{item.barcode}</TableCell>
                <TableCell>{item.vendor_name}</TableCell>
                <TableCell>{item.vendor_cost}</TableCell>
                <TableCell>79</TableCell>
                <TableCell>link</TableCell>
            </TableRow>
        ))}
    </TableBody>
    </Table>
                      
            </div>
          </Fade>
        </Modal>
    </div>)
}
}