import React, {Component} from 'react'
import axios from 'axios';
import {SinoBatchProducts} from './propsFunctions'
import {Title} from './../list/propslist' 

export default class Product extends Component{
    constructor(props){
        super(props);
        this.state={
            products:[]
        }
    }
    
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/sino_products').then((res) => {
            this.setState({ products: res.data.products, loading: false })
            console.log(this.state.products)
        })
    }
render(){
    let {products} = this.state
    return(<div>
        <Title listTitle="Batch List" />
        <SinoBatchProducts batchProducts={products}/>
    </div>)
}
}