import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './purchased.css';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles, Badge, Card, CardContent, CardActions, Grid, IconButton, TextField, MenuItem } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Button from '@material-ui/core/Button';
import ShoppingBasketRounded from '@material-ui/icons/ShoppingBasketRounded';
import Alert from '@material-ui/lab/Alert';

const ExpansionPanelSummaryStyle = withStyles({
    root: {
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        minHeight: '50px !important',
        '&$expanded': {
            minHeight: '50px !important',
        },
    }
})(MuiExpansionPanelSummary);
const ExpansionPanelSummaryStyleNested = withStyles({
    root: {
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        minHeight: '50px !important',
        backgroundColor: '#f9f9f9',
        '&$expanded': {
            minHeight: '50px !important',
        },
    }
})(MuiExpansionPanelSummary);
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: '#f9f9f9'
    }
}))(TableCell);
const StyledTableCellTD = withStyles((theme) => ({
    body: {
        padding: '10px 26px 10px 16px'
    }
}))(TableCell);
const StyledTableCellCart = withStyles((theme) => ({
    body: {
        padding: '5px 26px 5px 16px'
    }
}))(TableCell);
const useStyles = makeStyles((theme) => ({

    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));
const useStylesCartView = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    root2: {
        flexGrow: 1,
    },
});

export default function PurchasedAccordian(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [nestedExpanded, setNestedExpanded] = React.useState(false);
    const [childExpanded, setChildExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    const nestedHandleChange = (panel) => (event, isExpanded) => {
        setNestedExpanded(isExpanded ? panel : false);
    };
    const nestedChildHandleChange = (panel) => (event, isExpanded) => {
        setChildExpanded(isExpanded ? panel : false);
    };
    const groups = props.allItems.filter((item) => item.product_category === props.item.product_category);
    //console.log("groups ==> ", groups)
    // const cartItems = props.cart.filter((cartItem) => cartItem.product_barcode === products.product_barcode);
    // console.log("cartItems ==> ", cartItems)
    return (
        <ExpansionPanel expanded={expanded === props.item.product_uuid} onChange={handleChange(props.item.product_uuid)}>
            <ExpansionPanelSummaryStyle
                expandIcon={<ExpandMoreIcon />}
                aria-controls={"panel-content" + props.keyIndex}
                id={"panel-header" + props.keyIndex}
            >
                <Typography className={classes.heading}>{props.item.product_category}</Typography>
                <Typography className={classes.secondaryHeading}>{props.item.product_barcode}</Typography>
            </ExpansionPanelSummaryStyle>
            <ExpansionPanelDetails>
                {/* Nested */}
                <div style={{ width: '100%' }}>
                    {groups.map((item, index) => {
                        const products = groups.filter((product) => product.product_description === item.product_description && product.uuid === item.uuid);
                        console.log("Products ==> ", products);
                        return (
                            <div style={{ width: '100%', margin: '10px 0' }}>
                                <ExpansionPanel key={item.uuid} expanded={nestedExpanded === item.uuid} onChange={nestedHandleChange(item.uuid)}>
                                    <ExpansionPanelSummaryStyleNested
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls={"panel-content" + item.uuid + "nested"}
                                        id={"panel-header" + item.uuid + "nested-child"}
                                    >
                                        <Typography className={classes.heading}>{item.product_description}</Typography>
                                        <Typography className={classes.secondaryHeading}>Group</Typography>
                                    </ExpansionPanelSummaryStyleNested>
                                    <ExpansionPanelDetails>
                                        {/* Nested Child */}
                                        {products.map((product, productIndex) => {
                                            return (
                                                <div style={{ width: '100%', margin: '10px 0' }}>
                                                    <ExpansionPanel key={product.uuid + "child" + productIndex} expanded={childExpanded === product.uuid + "child" + productIndex} onChange={nestedChildHandleChange(product.uuid + "child" + productIndex)}>
                                                        <ExpansionPanelSummaryStyleNested
                                                            expandIcon={<ExpandMoreIcon />}
                                                            aria-controls={"panel-content" + product.uuid + "nested-child"}
                                                            id={"panel-header" + product.uuid + "nested-child"}
                                                        >
                                                            <Typography className={classes.heading}>{product.product_name}</Typography>
                                                            <Typography className={[classes.secondaryHeading, "customHeading"]}>Product</Typography>
                                                            <Typography className={classes.secondaryHeading}>{product.product_barcode}</Typography>
                                                        </ExpansionPanelSummaryStyleNested>
                                                        <ExpansionPanelDetails>
                                                            <TableContainer component={Paper}>
                                                                <Table aria-label="simple table" size="small">
                                                                    <TableHead>
                                                                        <TableRow>

                                                                            <StyledTableCell>UUID</StyledTableCell>
                                                                            <StyledTableCell>Item</StyledTableCell>
                                                                            <StyledTableCell>Barcode</StyledTableCell>
                                                                            <StyledTableCell>Cost</StyledTableCell>
                                                                        </TableRow>
                                                                    </TableHead>
                                                                    <TableBody>
                                                                        {product.item.map((item, index) => {
                                                                           //const cart = props.cart.filter((cartItem) => cartItem.uuid === item.uuid);
                                                                            return (
                                                                                <TableRow key={item.uuid}>
                                                                                    <StyledTableCellTD>{item.uuid}</StyledTableCellTD>
                                                                                    <StyledTableCellTD>{item.product_item}</StyledTableCellTD>
                                                                                    <StyledTableCellTD>{item.product_item_barcode}</StyledTableCellTD>
                                                                                    <StyledTableCellTD>{item.product_item_cost}</StyledTableCellTD>
                                                                                </TableRow>
                                                                            );
                                                                        })}
                                                                    </TableBody>
                                                                </Table>
                                                            </TableContainer>
                                                        </ExpansionPanelDetails>
                                                    </ExpansionPanel>
                                                </div>
                                            );
                                        })}
                                        {/* Nested Child */}
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </div>
                        );
                    })}
                </div>
                {/* Nested */}
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}