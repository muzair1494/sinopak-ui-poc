import React from 'react';
import axios from 'axios';
import { Title } from '../../list/propslist';
import StoredAccordian from './propsFunction';


class StoredItems extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            loading: true,
            sortedList: [],
            collectedCategory: []
        };
    }
    componentDidMount() {
        axios.get('http://demo1951402.mockable.io/item').then((response) => {
            const filteredResponse = response.data.Product_items.filter((data, ind) => data.type === 'stored');
            this.setState({ list: filteredResponse, loading: false });
            let setArr = [];
            for (var i = 0; i < filteredResponse.length; i++) {
                setArr.push(filteredResponse[i].product_category);
            }
            let set = new Set(setArr);
            var mySet = Array.from(set);
            const sortedList = filteredResponse.filter((item, index) => item.product_category === mySet[index]);
            this.setState({ sortedList, loading: false })
        }).catch(err => console.log('Error in Sales Api ==> ', err));
    }
    render() {
        const { list, sortedList, loading } = this.state;
        return (
            <div>
                <div className="listTitle">
                    <Title sty listTitle="Stored Items" />
                </div>
                <div style={{ width: '100%' }}>
                    {sortedList.map((item, index) => {
                        return (
                            <StoredAccordian
                                allItems={list}
                                key={index}
                                keyIndex={index}
                                item={item} />
                        );
                    })}
                    {loading && <div className="loadingTr">Loading ...</div>}
                </div>
            </div>
        );
    }
}

export default StoredItems;