import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ProductList from './components/product/productList';
import VendorList from './components/vendor/vendorList';
import List from './components/list/list';
import Header from './components/header/header';
import BatchProducts from './components/batch_products/filteredBatchItems';
import PurchaseOrder from './components/purchaseOrder/index';
import InventoryList from './components/inventoryProduct/index';
import AddTabs from './components/tabs/addForms/index';
import AddItemTabs from './components/items/index';
import GRN from './components/GRN/index';
import { Provider } from 'react-redux';
import configureStore from './store';
import Login from './components/Login';
import Users from './components/users/index';
import Organization from './components/organization/dataView';

function App() {
  //console.log("Route Name ==> ", window.location.pathname);
  return (
    <div className="App">
      <Provider store={configureStore()}>
          <Router>
            {window.location.pathname === "/login" || window.location.pathname === '/' ? null : <Header />}
            <Switch>
              {/* <Route path="/" exact component={List} /> */}
              <Route path="/" exact component={Login} />
              <Route path="/login" exact component={Login} />
              <Route path="/users" exact component={Users} />
              <Route path="/organization" exact component={Organization} />
              <Route path="/purchaseOrder" exact component={PurchaseOrder} />
              <Route path="/GRN" exact component={GRN} />
              <Route path="/inventoryList" exact component={InventoryList} />
              <Route path="/products" exact component={ProductList} />
              <Route path="/vendors" exact component={VendorList} />
              <Route path="/AddTabs" exact component={AddTabs} />
              <Route path="/items" exact component={AddItemTabs} />
            </Switch>
          </Router>
      </Provider>
    </div>
  );
}
export default App;
