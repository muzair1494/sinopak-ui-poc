import {FETCH_PRODUCTS_PENDING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR,
    FETCH_PRODUCTS_PENDINGV1, FETCH_PRODUCTS_SUCCESSV1, FETCH_PRODUCTS_ERRORV1,
        FETCH_GRN_PENDING,FETCH_GRN_SUCCESS, FETCH_GRN_ERROR ,GET_POS
} from './constants';
import axios from 'axios';
import {actionDispatch} from './ret_action_obj'

export const simpleAction = () => dispatch => {
    dispatch({
     type: 'SIMPLE_ACTION',
     payload: 'result_of_simple_action'
    })
   }

export const getPurchasedProducts = ()  => {
    return function(dispatch) {
    dispatch(actionDispatch(FETCH_PRODUCTS_PENDING,true))
    return axios.get("http://demo1951402.mockable.io/products")
        .then(res=>dispatch(actionDispatch(FETCH_PRODUCTS_SUCCESS,res.data.products)))
        .catch(err=>dispatch(actionDispatch(FETCH_PRODUCTS_ERROR,err)))
    };
}

export const getGrnList =()=>{
    return function(dispatch) {
        dispatch(actionDispatch(FETCH_GRN_PENDING,true))
        return axios.get("http://demo1951402.mockable.io/purchaseOrder")
            .then(res=>dispatch(actionDispatch(FETCH_GRN_SUCCESS,res.data.Purchase_Orders)))
            .catch(err=>dispatch(actionDispatch(FETCH_GRN_ERROR,err)))
        };
}

export const getProducts =()=>{
    return function(dispatch) {
    dispatch(actionDispatch(FETCH_PRODUCTS_PENDINGV1,true))
    return axios.get("http://demo1951402.mockable.io/productv2")
        .then(res=>dispatch(actionDispatch(FETCH_PRODUCTS_SUCCESSV1,res.data.products)))
        .catch(err=>dispatch(actionDispatch(FETCH_PRODUCTS_ERRORV1,err)))
    }; 
}

export const getPOS =(value)=>{
    return function(dispatch) {
        dispatch(actionDispatch(GET_POS,value))
    }
}

