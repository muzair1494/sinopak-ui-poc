export const FETCH_PRODUCTS_PENDING = 'FETCH_PRODUCTS_PENDING';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';
export const FETCH_PRODUCTS_PENDINGV1 = 'FETCH_PRODUCTS_PENDINGV1';
export const FETCH_PRODUCTS_SUCCESSV1 = 'FETCH_PRODUCTS_SUCCESSV1';
export const FETCH_PRODUCTS_ERRORV1 = 'FETCH_PRODUCTS_ERRORV1';
export const FETCH_GRN_PENDING = 'FETCH_GRN_PENDING';
export const FETCH_GRN_SUCCESS = 'FETCH_GRN_SUCCESS';
export const FETCH_GRN_ERROR = 'FETCH_GRN_ERROR';
export const GET_POS = "GET_POS"

export const fetchProductsPending=()=> {
    return {
        type: FETCH_PRODUCTS_PENDING
    }
}

export const  fetchProductsSuccess=(products)=> {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        products: products
    }
}

export const  fetchProductsError=(error)=> {
    return {
        type: FETCH_PRODUCTS_ERROR,
        error: error
    }
}
