import {FETCH_PRODUCTS_PENDING, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR,
    FETCH_PRODUCTS_PENDINGV1, FETCH_PRODUCTS_SUCCESSV1, FETCH_PRODUCTS_ERRORV1,
    FETCH_GRN_PENDING,FETCH_GRN_SUCCESS, FETCH_GRN_ERROR, GET_POS
} from './../actions/constants';


const initialState = {
  pending: false,
  products: [],
  error: null,
  pending_GRN: false,
  GRN_List: [],
  err_GRN: null,
  result:'',
  pendingv1: false,
  productsv1: [],
  errorv1: null,
  POS:''
}
export default (state = initialState, action) => {
    switch (action.type) {
      case 'SIMPLE_ACTION':
      return {
       result: action.payload
      }
      case FETCH_PRODUCTS_PENDING: 
          return {
              ...state,
              pending: true
          }
      case FETCH_PRODUCTS_SUCCESS:
          return {
              ...state,
              pending: false,
              products: action.payload
          }
      case FETCH_PRODUCTS_ERROR:
          return {
              ...state,
              pending: false,
              error: action.error
          }
          case FETCH_PRODUCTS_PENDINGV1: 
          return {
              ...state,
              pendingv1: true
          }
      case FETCH_PRODUCTS_SUCCESSV1:
          return {
              ...state,
              pendingv1: false,
              productsv1: action.payload
          }
      case FETCH_PRODUCTS_ERRORV1:
          return {
              ...state,
              pendingv1: false,
              errorv1: action.error
          }
      case FETCH_GRN_PENDING: 
          return {
              ...state,
              pending_GRN: true
          }
      case FETCH_GRN_SUCCESS:
          return {
              ...state,
              pending_GRN: false,
              GRN_List: action.payload
          }
      case FETCH_GRN_ERROR:
          return {
              ...state,
              pending_GRN: false,
              err_GRN: action.error
          }
     case GET_POS:
        return {
            ...state,
            POS: action.payload
        }
     default:
      return state
    }
   }